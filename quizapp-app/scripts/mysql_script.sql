use mfriend_db;

ALTER TABLE booking ADD CONSTRAINT ca_id FOREIGN KEY ( car_id )
 REFERENCES spring_db.car ( id )
 ON DELETE NO ACTION;
 
 ALTER TABLE booking ADD CONSTRAINT user_id FOREIGN KEY ( user_id )
 REFERENCES spring_db.user ( id )
 ON DELETE NO ACTION;
 
 ALTER TABLE booking DROP FOREIGN KEY FKkgseyy7t56x7lkjgu3wah5s3t;
 
CREATE TABLE message 
( 
  id INT(10) NOT NULL 
, username VARCHAR(255) NULL 
, email VARCHAR(255) NOT NULL 
, full_name VARCHAR(255) NOT NULL 
, message VARCHAR(255) NOT NULL 
, message_type VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE person 
( 
  ID INT(10) NOT NULL 
, first_name VARCHAR(255) NOT NULL 
, last_name VARCHAR(255) NOT NULL 
, city VARCHAR(255) NOT NULL 
, address VARCHAR(255) NOT NULL 
, image MEDIUMBLOB NOT NULL 
, CONSTRAINT PRIMARY KEY ( ID ) );

CREATE TABLE role 
( 
  id INT(10) NOT NULL 
, role VARCHAR(255) NOT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user 
( 
  id BIGINT(19) NOT NULL 
, username VARCHAR(255) NOT NULL 
, password VARCHAR(255) NOT NULL 
, email VARCHAR(255) NOT NULL 
, mobile VARCHAR(255) 
, first_name VARCHAR(255) 
, last_name VARCHAR(255)  
, city VARCHAR(255) 
, address VARCHAR(255)  
, enabled BIT NOT NULL 
, reset_code VARCHAR(255) NULL 
, reset_status BIT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

ALTER TABLE user modify id BIGINT(19) NOT NULL AUTO_INCREMENT;

ALTER TABLE user modify mobile VARCHAR(255);
ALTER TABLE user modify first_name VARCHAR(255);
ALTER TABLE user modify last_name VARCHAR(255);
ALTER TABLE user modify city VARCHAR(255);
ALTER TABLE user modify address VARCHAR(255);

ALTER TABLE user ADD news_letter  tinyint(1) default 0;

insert into user 
values(1, 'ajay', 'ajay', 'ajay@ajay.com', '9833229219', 'Aj', 'Rj', 'Mumbai', 'Mum', 1, '',false);
insert into user 
values(2, 'abhay', 'abhay', 'abhay@ajay.com', '9923345564', 'Aj', 'Rj', 'Mumbai', 'Mum', 1, '',false);

update user set news_letter=0;

CREATE TABLE user_role 
( 
  id INT(10) NOT NULL 
, user_id INT(10) NOT NULL 
, role_id INT(10) NOT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );
ALTER TABLE user_role modify id INT(10) NOT NULL AUTO_INCREMENT;

insert into role values(1, 'ADMIN');
insert into role values(2, 'USER');
insert into role values(4, 'TEST');

insert into user_role values(1, 1, 1);
insert into user_role values(2, 2, 1);
update user_role set role_id = 2 where id=2;

insert into message values (1, 'ajay', 'ajay@ajay.com', 'Aj RJ', 'Hey just wanted to say Hi!!', 'CONTACT_US');

CREATE TABLE business 
( 
  id BIGINT(19) NOT NULL 
, biz_name VARCHAR(255) NOT NULL 
, biz_type VARCHAR(255) NOT NULL 
, speciality VARCHAR(20000) NOT NULL 
, description VARCHAR(20000)
, area VARCHAR(255) NOT NULL 
, full_address VARCHAR(255) NOT NULL 
, email VARCHAR(255) NOT NULL 
, mobile VARCHAR(255)
, work_number VARCHAR(255)
, website VARCHAR(255)
, poster LONGBLOB  
, thumb LONGBLOB
, map_location VARCHAR(255) 
, CONSTRAINT PRIMARY KEY ( id ) );

ALTER TABLE business modify full_address VARCHAR(255);
ALTER TABLE business modify id BIGINT(19) NOT NULL AUTO_INCREMENT;

ALTER TABLE business ADD rating int(5) unsigned;

ALTER TABLE business ADD verified tinyint(1) default 0;

ALTER TABLE business ADD quantifier CHARACTER(1);

insert into business values
(1,'Saneesh Inc.','Service','Speciality Details','Biz Description.','Vashi',
'202, A Wing, Opp Subhash Chandra Bose, Mahada Colony, Vashi - 400223','saneesh@saneesh.com','3366335464',
'02223345534','mfriend.in'
,LOAD_FILE('D:\workarea\Repo\mfriend\test\big.jpg')
,LOAD_FILE('D:\workarea\Repo\mfriend\test\small.jpg')
,'34345345,234234', 3, 1, 'P');
insert into business values
(2,'Abhay Inc.','Design IT','Speciality Details for Anark web site','Biz Description.','Sion',
'503, Sriram CHS','saneesh@saneesh.com','8899887766',
'02226667343','anark.in'
,LOAD_FILE('D:\workarea\Repo\mfriend\test\big.jpg')
,LOAD_FILE('D:\workarea\Repo\mfriend\test\small.jpg')
,'34345345,234234', 3, 0, 'B');

delete from business where id=1;

update business set description='Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably havent heard of them accusamus labore sustainable VHS.' where id=1;

--linux cpanel = /home/mfriend/temp/big.jpg
--linux cpanel = /home/mfriend/temp/small.jpg
-- windows image path = F:/workarea/Repository/GitLab/mfriend/test/big.jpg
-- windows image path = F:/workarea/Repository/GitLab/mfriend/test/small.jpg


--To Drop Column
--ALTER TABLE business DROP COLUMN quantifier;

--to Update records
--update business set rating=3;

--To rename column name, first old name, second new name and the new datatype
--ALTER TABLE xyz CHANGE manufacurerid manufacturerid INT;

select u.username,u.password, u.enabled from user u where u.username='saneesh';

select u.username, r.role from user u, role r, user_role ur where u.username ='ajay' and u.id=ur.user_id and r.id=ur.role_id;