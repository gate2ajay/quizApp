create database spring_db;
use spring_db;
CREATE TABLE message 
( 
  id INT(10) NOT NULL 
, username VARCHAR(255) NULL 
, email VARCHAR(255) NOT NULL 
, full_name VARCHAR(255) NOT NULL 
, message VARCHAR(255) NOT NULL 
, message_type VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE role 
( 
  id INT(10) NOT NULL 
, role VARCHAR(255) NOT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user 
( 
  id BIGINT(19) NOT NULL 
, username VARCHAR(255) NOT NULL 
, password VARCHAR(255) NOT NULL 
, email VARCHAR(255) NOT NULL 
, mobile VARCHAR(255) NOT NULL 
, first_name VARCHAR(255) NOT NULL 
, last_name VARCHAR(255) NOT NULL 
, city VARCHAR(255) NOT NULL 
, address VARCHAR(255) NOT NULL 
, enabled BIT NOT NULL 
, reset_code VARCHAR(255) NULL 
, reset_status BIT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user_role 
( 
  id INT(10) NOT NULL 
, user_id INT(10) NOT NULL 
, role_id INT(10) NOT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

insert into user 
values(1, 'ajay', 'ajay', 'ajay@ajay.com', '9833229219', 'Aj', 'Rj', 'Mumbai', 'Mum', 1, '',false);
insert into user 
values(2, 'abhay', 'abhay', 'abhay@ajay.com', '9923345564', 'Aj', 'Rj', 'Mumbai', 'Mum', 1, '',false);
insert into user 
values(3, 'shalom', 'shalom', 'shalom@shalom.com', '9923345564', 'Aj', 'Rj', 'Mumbai', 'Mum', 1, '',false);

insert into role values(1, 'ADMIN');
insert into role values(2, 'USER');
insert into role values(4, 'TEST');

insert into user_role values(1, 1, 1);
insert into user_role values(2, 2, 1);
update user_role set role_id = 2 where id=2;

insert into message values (1, 'ajay', 'ajay@ajay.com', 'Aj RJ', 'Hey just wanted to say Hi!!', 'CONTACT_US');

UPDATE role r SET r.id=3, r.role='PROF' WHERE r.id=4;

create table question_set
(
  id int not null,
  question_set VARCHAR(1000),
  CONSTRAINT PRIMARY KEY(id)
);

insert into question_set values(1, 'science');

CREATE TABLE qa 
( 
  id BIGINT(19) NOT NULL 
, question VARCHAR(1000) NOT NULL 
, answer VARCHAR(5000) NOT NULL 
, question_set int
, CONSTRAINT PRIMARY KEY ( id ) 
, FOREIGN KEY (question_set) REFERENCES question_set(id));

insert into qa values(1, 'What is the English quote used for typing practise?', 'The quick brown fox jumped over a lazy dog.', 1);
insert into qa values(2, 'What do you understand by precipitation?', 'Test text to check the correctness of automatic checker.', 1);
insert into qa values(3, 'what does the stream of Biology about in science?', 'This is about Science and it is good ;-)', 1);
insert into qa values(4, 'What is photosynthesis?', 'The process of breaking the elements from leaves to bring forth food to tree is called photosynthesis.', 1);
insert into qa values(5, 'Explain amoeba?', 'Shapeless creature of the world is called Amoeba.', 1);









--linux cpanel = /home/mfriend/temp/big.jpg
--linux cpanel = /home/mfriend/temp/small.jpg
-- windows image path = F:/workarea/Repository/GitLab/mfriend/test/big.jpg
-- windows image path = F:/workarea/Repository/GitLab/mfriend/test/small.jpg


--To Drop Column
--ALTER TABLE business DROP COLUMN quantifier;

--to Update records
--update business set rating=3;

--To rename column name, first old name, second new name and the new datatype
--ALTER TABLE xyz CHANGE manufacurerid manufacturerid INT;