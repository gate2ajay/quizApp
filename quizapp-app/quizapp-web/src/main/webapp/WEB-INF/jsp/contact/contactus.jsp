<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	    <link  href="css/font-awesome.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="images/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="..." class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		 <div class="clearfix"></div>
		 <div class="container">
		 	<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  
				</div>
			
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					
					<li><a href="index.html">Home</a></li>
					<li><a href="newsfeed.html">News Feed</a></li>
					<li><a href="locate.html">Locate</a></li>
					<li><a href="aboutus.html">About Us</a></li>
					<li class="active"><a href="contactus.html">Contact Us</a></li>
					</li>
				  </ul>
				  
				  
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div> 
		 
		 
		 <div class="container maincontent">
		 	<div class="row">
			 	<div class="col-md-12 well">
					<h3>Contact Us</h3>
					<div class="col-md-5">
						<address>
						  <h3>m'friends</h3><br>
						  AKALI F4 DEEP GEET C.H.S,<br>
						  PLOT No.10/6, SECTOR-15, NERUL EAST,<br>
						  NAVI MUMBAI-400706<br>
						  <abbr title="Phone">P:</abbr> (123) 456-7890
						</address>
						
						<address>
						  <strong><a href="www.mfriend.in"> www.mfriend.in</a></strong><br>
						  <a href="mailto:mfriendhere@gmail.com"> mfriendhere@gmail.com</a><br>
						  <strong>Contact No: +91 80807 30308</strong>
						</address>
					</div>
					<div class="col-md-7">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1885.8252761099209!2d73.01908815793763!3d19.03511408232248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c3c3d7ac515f%3A0x3962ef5a267b9f1e!2sSector+15%2C+Nerul%2C+Navi+Mumbai%2C+Maharashtra+400706!5e0!3m2!1sen!2sin!4v1465233862247" width="600" height="350" frameborder="0" style="border:1px solid #999;" allowfullscreen></iframe>
					</div>
				</div>
				
			</div>	
		 </div>
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>
 


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Login </h4>
      </div>
      <div class="modal-body">
        <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>

  	<button type="reset" class="btn btn-warning " data-dismiss="modal">Reset</button> <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>

</form>
      </div>
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	
	<!-- Later -->
	<form:form method="post" action="contactUs.htm" modelAttribute="feedback">
		<label for="name">Full name</label><form:input type="text" name="name" path="fullName" id="name" size="40" maxlength="100" value="" class="text" ></form:input><br />
		<label for="email">Email</label><form:input type="text" name="email" path="email" id="email" size="40" maxlength="100" value="" class="text" ></form:input><br />
		<label for="message">Message</label><form:textarea name="message" path="message" id="message" cols="60" rows="8" ></form:textarea><br /><br />
		<form:input type="hidden" name="messageType" path="messageType" id="messageType" size="40" maxlength="100" value="contact" ></form:input>
		Are you human?
		<%
		 ReCaptcha c = ReCaptchaFactory.newReCaptcha("6Lec_RQTAAAAABh0VoSKY3RVDrfTOkzbrf5MEF3D", "test", false);
		out.print(c.createRecaptchaHtml(null, null));
		%>
	
		<br />
		<input type="submit" value="Submit" />
	</form:form>
	
  </body>
</html>