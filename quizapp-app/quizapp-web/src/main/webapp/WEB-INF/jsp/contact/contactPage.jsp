<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<html>
<head>
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<h1>Title : ${title}</h1>
	<h1>Message : ${message}</h1>
	
	<c:if test="${success} == true">Message Posted Successfully!!!</c:if>
	<c:if test="${success eq false} == true">Message could not be posted successfully!!!</c:if>
	
<form:form method="post" action="contactUs.htm" modelAttribute="feedback">
	<label for="name">Full name</label><form:input type="text" name="name" path="fullName" id="name" size="40" maxlength="100" value="" class="text" ></form:input><br />
	<label for="email">Email</label><form:input type="text" name="email" path="email" id="email" size="40" maxlength="100" value="" class="text" ></form:input><br />
	<label for="message">Message</label><form:textarea name="message" path="message" id="message" cols="60" rows="8" ></form:textarea><br /><br />
	<form:input type="hidden" name="messageType" path="messageType" id="messageType" size="40" maxlength="100" value="contact" ></form:input>
	Are you human?
	<%
	 ReCaptcha c = ReCaptchaFactory.newReCaptcha("6Lec_RQTAAAAABh0VoSKY3RVDrfTOkzbrf5MEF3D", "test", false);
	out.print(c.createRecaptchaHtml(null, null));
	%>

	<br />
	<input type="submit" value="Submit" />
</form:form>

	<sec:authorize access="hasRole('ROLE_USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	
</body>
</html>