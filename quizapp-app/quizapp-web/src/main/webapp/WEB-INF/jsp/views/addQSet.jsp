<%@ include file="../head.jsp" %><!-- Add User -->
<script src="static/js/views/addQSet.js"></script>
<script src="static/js/ajaxCallBack.js"></script>
<%-- <script type="text/javascript" src="${cp}/dwr/interface/addQuestionSet.js"></script> --%>
        
    </head>
    
    <body>
        <div id="blockctnr">
           <div id="xbutton"><h2><a href="${cp}/">X</a></h2></div>
            <div id="blockin">
                <h1>Professor Page: Add Question Set</h1>
                    <div id="blockz">
                    	<!-- Content goes here -->
                    	<div id="alertBox" class="alert alert-dismissable alert-warning" style="display: none">
							<button type="button" class="close" data-hide="alert" id="closeAlert">&times;</button>
							<b><span id="alertType">Warning !</span></b> <span id="alertDetail">*Add number of questions to start adding Questions.</span>
						</div>
                    	<span style="color: red;font-weight: bold;">*</span><span style="color: floralwhite;">Add number of questions to start adding Questions.</span>
                    	
                    	
                    	
                    	<div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <h3>Question Set: <small>Fields marked with an asterik (*) are required</small></h3>
                                </div>
                        </div><!-- row 1 end -->
                    	<form:form id="qSet" method="post" action="setPaper.htm" modelAttribute="qSet">
                    	<div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="row form-group">
                                        <div class="col-md-5 text-right"><label for="noQ">No of Questions: <span class="asterik">*</span></label><br><em>Max 10 questions.</em></div>
                                        <div class="col-md-7">
                                            <form:input class="form-control" type="text" name="noQ" path="noOfQuestion" placeholder="No. of Questions" id="noOfQuestion" size="10" maxlength="10" value="" ></form:input>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-5 text-right"><label for="qSet">Question set: <span class="asterik">*</span></label></div>
                                        <div class="col-md-7">
                                        	
                                       	<form:select name="subject" path="subject" id="subject">
					                        <form:option value="standard">-Select Subject-</form:option>
					                        <form:option value="science">Science</form:option>
					                        <form:option value="english">Maths</form:option>
					                        <form:option value="history">History</form:option>
					                    </form:select>
                                        	
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-5 text-right"><label for="totalMarks">Total Marks for question set: <span class="asterik">*</span></label><em>Will be divided according to marks assigned to number of questions.</em></div>
                                        <div class="col-md-7">
                                        	<form:input name="totalMarks" placeholder="Total marks" class="form-control" path="totalMarks" id="totalMarks"></form:input>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                     <div id="dynamic" class="">
                                    <%--<div class="row form-group">
                                    		<div class="col-md-1 text-right"><label for="qNo">Q: <span class="asterik">*</span></label></div>
                                    	</div>
	                                    <div class="row form-group">
	                                        <div class="col-md-8 text-left"><textarea class="form-control textarea" type="text" name="question" id="question" value="" rows="3"></textarea></div>
	                                        <div class="col-md-2 text-right"><label for="wt">Weight: </label></div>
	                                        <div class="col-md-2">
	                                            <input class="form-control" type="text" name="weight" id="weight" size="5" maxlength="5" value="" ></input>
	                                        </div>
	                                    </div>
	                                    
	                                    <div class="row form-group">
	                                        <div class="col-md-2 text-right"><label for="wt">Answer: </label></div>
	                                        <div class="col-md-10">
	                                            <div class="col-md-10 text-left"><textarea class="form-control textarea" type="text" name="answer" id="answer" value="" rows="3"></textarea></div>
	                                        </div>
	                                    </div>
	                                    
	                                    
	                                    
	                                    
	                                    <div id="qTags" class="">
	                                    	<div class="row">
		                                    	<div class="col-xs-12 col-sm-12 col-md-4">
			                                    	<div class="col-md-8 text-right"><label for="word">valueVerybigwordHere </label></div>
			                                        <div class="col-md-4">
			                                            <input class="form-control" type="text" name="weight" id="weight" value="" ></input>
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-12 col-md-4">
			                                    	<div class="col-md-8 text-right"><label for="word">valueHere </label></div>
			                                        <div class="col-md-4">
			                                            <input class="form-control" type="text" name="weight" id="weight" size="5" maxlength="5" value="" ></input>
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-12 col-md-4">
			                                    	<div class="col-md-8 text-right"><label for="word">valueHere </label></div>
			                                        <div class="col-md-4">
			                                            <input class="form-control" type="text" name="weight" id="weight" size="5" maxlength="5" value="" ></input>
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-12 col-md-4">
			                                    	<div class="col-md-8 text-right"><label for="word">valueHere </label></div>
			                                        <div class="col-md-4">
			                                            <input class="form-control" type="text" name="weight" path="weight" id="weight" size="5" maxlength="5" value="" ></input>
			                                        </div>
			                                    </div>
		                                    </div>
	                                    </div>
	                                    
	                                    <div class="row form-group">
                                    		<div class="col-md-1 text-right"><label for="qNo">Q: <span class="asterik">*</span></label></div>
                                    	</div>
	                                    <div class="row form-group">
	                                        <div class="col-md-8 text-left"><form:textarea class="form-control textarea" type="text" name="questions" path="questions" id="questions" value="" rows="3"></form:textarea></div>
	                                        <div class="col-md-2 text-right"><label for="wt">Weight: </label></div>
	                                        <div class="col-md-2">
	                                            <form:input class="form-control" type="text" name="weight" path="weight" id="weight" size="5" maxlength="5" value="" ></form:input>
	                                        </div>
	                                    </div>
                                    </div>--%>
                                   </div> 
                                    <div class="row form-group">
                                    	<div class="col-md-5 text-center"></div>
	                                    <div class="col-md-7">
		                                    <button class="btn btn-primary btn-lg" type="submit">Submit</button>
		                                </div>
	                                </div> 
                                
                            </div><!-- row 2 end -->
                    	</form:form>
                    	
                    	
                    	
                    	
                    	
                    	
                    	
                    	
                    	
                    </div>
                    <!-- <div id="blockzz">    
                        <ul>
                        <li id="text1">View Result</li>
                         
                        <li><select name=subject>
                        <option value="standard">-Select Subject-</option>
                        <option value="Science">Science</option>
                        <option value="Maths">Maths</option>
                        <option value="English">English</option>
                         </select></li>
                        
                         
                        <li id="btn">SUBMIT</li>
                        </ul>
                    </div> -->
            </div>
        </div>
        <div id="footer">
            <div id="fotl"><h6>Copyright at school</h6></div>
            <div id="fotr"><h6>info@school.com</h6></div>
        </div>
        
        
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	<script src="static/js/lib/bootstrap-select.js"></script>
    </body>
    
    
        
</html>