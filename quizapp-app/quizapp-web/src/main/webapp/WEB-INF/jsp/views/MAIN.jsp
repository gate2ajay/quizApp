<%@ include file="../head.jsp" %><!-- Main Page -->
        
    </head>
    
    <body>
        <div id="block">
            <div id="blockin">
                <div><h1><a class="roleType" id="adminLink" data-target="#myModal" data-toggle="modal">ADMIN</a></h1></div>
                <div><h1><a class="roleType" id="staffLink" data-target="#myModal" data-toggle="modal">STAFF</a></h1></div>
                <div><h1><a class="roleType" id="studentLink" data-target="#myModal" data-toggle="modal">STUDENT</a></h1></div>
                <sec:authorize access="isAuthenticated()">
					<c:set var="authenticated" scope="page">YES</c:set>
				</sec:authorize>
                <c:choose>
				  <c:when test= "${authenticated == 'YES'}">
					<c:if test="${pageContext.request.userPrincipal.name != null}">
					<div style="float:left;margin-top: 12px">
						<span style="margin-top:10px">User : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit();"> Logout</a></span>
					</div>
					</c:if>
				  </c:when>
				  <c:otherwise>
				  <div style="margin-top: 12px">
				    <a class="btn btn-primary" style="top:10px" data-target="#myModal" data-toggle="modal">Login</a>
					<a class="btn btn-warning"  href="registerPage.htm?new">New User</a>
				  </div>
				  </c:otherwise>
				</c:choose>
				<!-- Form to submit logout -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>
		<!-- End: Form to submit logout -->
            </div>
        </div>
        
        <!-- Common Login Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Login </h4>
		      </div>
		      <div class="modal-body">
		      	<c:url var="loginUrl" value="/login" />
			  	<form name="loginForm" action="${loginUrl}" method="POST">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Username</label>
				    <input type="text" name="username" class="form-control" id="username" placeholder="Username">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
				  </div>
				  <div class="form-group">
				    <label for="rememberMe">Role Type: <span id="roleTypeLeable"></span></label>
				    <!-- <input type="checkbox" id="roleType" name="roleType"/> -->
				    <input type="hidden" id="roleTypeValue" name="roleTypeValue" value="" />
				  </div>
				  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				  <button type="reset" class="btn btn-warning">Reset</button> 
				  <button type="submit" class="btn btn-primary">Submit</button><!-- Once submitted successfully!! data-dismiss="modal" -->
		
				</form>
		      </div>
		     
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div>
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	<script src="static/js/lib/bootstrap-select.js"></script>
	<script src="static/js/views/main.js"></script>
	<script src="static/js/views/locate.js"></script>
    </body>
    
</html>