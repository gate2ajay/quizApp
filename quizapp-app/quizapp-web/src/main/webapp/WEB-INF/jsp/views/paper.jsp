<%@ include file="../head.jsp" %><!-- Main Page -->
        
    </head>
    
    <body>
        <div id="blockctnr">
        
        <sec:authorize access="isAuthenticated()">
					<c:set var="authenticated" scope="page">YES</c:set>
				</sec:authorize>
                <c:choose>
				  <c:when test= "${authenticated == 'YES'}">
					<c:if test="${pageContext.request.userPrincipal.name != null}">
					<div style="float:left;margin-top: 12px">
						<span style="margin-top:10px">User : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit();"> Logout</a></span>
					</div>
					</c:if>
				  </c:when>
				  <c:otherwise>
				  <div style="float:left;margin-top: 12px">
				    <a class="btn btn-primary" style="top:10px" data-target="#myModal" data-toggle="modal">Login</a>
					<!-- <a class="btn btn-warning"  href="registerPage.htm?new">New User</a> -->
				  </div>
				  </c:otherwise>
				</c:choose>
				<!-- Form to submit logout -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>
		<!-- End: Form to submit logout -->
        
           <div id="xbutton"><h2><a href="student_page.html">X</a></h2></div>
            <div id="blockin">
                <h1>SCIENCE PAPER</h1>
                    <div id="blockz">
                      <form:form id="submitPaper" method="POST" action="submitPaper.htm?qSet=${questionSet}" modelAttribute="data">
<!--Question Ans-->
					<c:forEach items="${questionList}" var="question" varStatus="theCount">
<!--    Q${theCount.count}--> <div id="question${theCount.count}" class="ansbox <c:choose>
		<c:when test="${theCount.count == 1}">visible</c:when>
		<c:otherwise>hidden</c:otherwise>
	</c:choose>">
                          <p>Q${theCount.count}. ${question}</p>
                          <div class="anspad"><form:textarea path="ans${theCount.count}" id="ans${theCount.count}" type="text"  rows="4" cols="50"></form:textarea></div>
                          <a id="prev${theCount.count}" href="javascript:void(0);" onClick="prevClicked(${theCount.count});">Prev</a> | <a id="next${theCount.count}" href="javascript:void(0);" onClick="nextClicked(${theCount.count});">Next</a>
                     </div>
                    </c:forEach>
                       
                       <div id="submitAns" class="hidden">
                       	<input type="submit" value="SUBMIT" />
                       </div>
                     </form:form>
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>
            </div>
        </div>
        <div id="footer">
            <div id="fotl"><h6>Copyright at school</h6></div>
            <div id="fotr"><h6>info@school.com</h6></div>
        </div>
        
        
    
    </body>
    
    
        
</html>