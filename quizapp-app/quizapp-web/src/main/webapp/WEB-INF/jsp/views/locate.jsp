<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends :: Locate</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">
	<link href="static/css/bootstrap-select.css" rel="stylesheet">

	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
	var postSaved = '${postSaved}';
    </script>
  </head>
  <body>
	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="static/img/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="..." class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		 <div class="clearfix"></div>
		 <%@ include file="../headNav.jsp" %>
		 <div class="container">
		 	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="static/img/banner01.jpg" alt="..." class="img-responsive">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  
</div>
		 </div>
		 <div class="spacer"></div>
		 <div class="container ">
		 	 <div class="row">
			 	<div class="col-md-3">
					<select class="form-control selectpicker">
					  <option>Mumbai</option>
					  <option>Pune</option>
					  <option>Nasik</option>
					</select>
				</div>
			 	<div class="col-md-7">
				<div class="form-group">
					<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
					<div class="input-group">
					  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Search ">
					  <div class="input-group-addon"><a href="javascript:;"><i class="fa fa-search"></i></a></div>
					</div>
				  </div>
				  </div>
	
				
						
			 </div>
		 </div>
		 <div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
		</div>
		 <div class="container ">
		 	 <div class="row geryBg">
			 	<div class="col-md-2 sortBg"><h4><i class="fa fa-sort"></i> Sort</h4>
				</div>
				<div class="col-md-10 sorting"><h5> <span><a href="javascript:;">Top Results</a></span> <span><a href="javascript:;">Location</a></span> <span><a href="javascript:;">Category</a></span> <span><a href="javascript:;">Price</a></span></h5>
				</div>
			 </div>
		 </div>		
		 
		 <div class="container maincontent">
		 	<div class="row">
				<div class="col-md-2">
					<div class="row sortBg">
						<h4>&nbsp;<i class="fa fa-filter"></i> Filter</h4>
					</div>
					<div class="row geryBg">
					<ul class="list-group">
					  <li class="list-group-item active">Area</li>
					  <li class="list-group-item">
						  <select class="selectpicker" multiple  data-width="fit" data-actions-box="true" title="Select City" data-size="5"  data-live-search="true">
								  <c:forEach items="${areaList}" var="area" varStatus="count">
								  <option  data-tokens="${area}">${area}</option>
									 </c:forEach>
								</select>
					  </li>
					  <li class="list-group-item active">Category</li>
					  <li class="list-group-item">
						  <select class="selectpicker" multiple  data-width="fit" data-actions-box="true" title="Select Category"  data-size="10">
							   <optgroup label="Food" >
									<option>Restaurants</option>
									<option>Fast food</option>
									<option>Caterers</option>
								  </optgroup>
								  <optgroup label="Shops">
									<option>General Stores</option>
									<option>Electrical / Electronics</option>
									<option>Textile</option>
									<option>Other Shops</option>
								  </optgroup>
								  <optgroup label="Real Estate" >
									<option>Real Estate</option>
								  </optgroup>
								  <optgroup label="Medicine" >
									<option>Ayurveda</option>
									<option>Hospitals / Clinics</option>
									<option>Doctors</option>
								  </optgroup>
								  <optgroup label="Education" >
									<option>Education</option>
								  </optgroup>
								  <optgroup label="Jobs" >
									<option>Jobs</option>
								  </optgroup>
								  <optgroup label="Finance" >
									<option>Finance</option>
								  </optgroup>
								  <optgroup label="Freelancers / Services" >
									<option>Freelancers / Services</option>
								  </optgroup>
								  <optgroup label="Community" >
									<option>Community</option>
								  </optgroup>
								  <optgroup label="Places of worship" >
									<option>Churches</option>
									<option>Mosques</option>
									<option>Temples</option>
								  </optgroup>
								  <optgroup label="B2B Firms" >
									<option>B2B Firms</option>
								  </optgroup>
								  <optgroup label="Others" >
									<option>Movies</option>
									<option>Media</option>
									<option>Others</option>
								  </optgroup>
							</select>
					  </li>
					</ul>
						
					</div>
					
				</div>
			 	<div class="col-md-7">
					<div class="locateMain">
						<c:forEach items="${businessList}" var="singleBusiness" varStatus="status">
						<div class="row">
							<div class="col-md-9">
								<div class="media btmBdrDot">
								  <div class="media-left">
									<a href="#">
									  <img width="129px" height="96px" class="media-object img-thumbnail" src="${cp}/getImage/${singleBusiness.bizId}?type=THUMB" alt="...">
									</a>
								  </div>
								  <div class="media-body locateListing">
									<h4 class="media-heading"><a href="locateInner.html">${singleBusiness.bizName}</a></h4>
										<p class="locatesmTitle">${singleBusiness.area}</p>
										<p class="locateaddress"><i class="fa fa-map-marker"></i><span id="fullAddress${status}">${singleBusiness.fullAddress}</span> | <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="${singleBusiness.fullAddress}">more</a></p>		
										<p class="locatephone"><i class="fa fa-phone-square"></i>${singleBusiness.workNumber}, ${singleBusiness.mobileNumber}</p>							
								  </div>
								</div>
							</div>
							<div class="col-md-3">
<p>								<a href="locateInner.htm?id=${singleBusiness.bizId}" class="btn btn-warning"><i class="fa fa-arrow-circle-right"></i> Get More Info</a></p>
<p class="locatesmTitle"><c:if test="${singleBusiness.verified == 1}"><i class="fa fa-check lightGreen"></i>Verified</c:if><c:if test="${singleBusiness.verified == 0}">Not Verified</c:if></p>
<p class="locatesmTitle"> ${singleBusiness.rating} <i class="fa fa-star-half-empty lightGreen"></i> Rating</p>
<p class="locatesmTitle">
	<c:choose>
		<c:when test="${singleBusiness.quantifier == 'P'}"><i class="fa fa-dot-circle-o lightGreen"></i>Premium</c:when>
		<c:when test="${singleBusiness.quantifier == 'M'}"><i class="fa fa-circle-o lightGreen"></i>Medium</c:when>
		<c:otherwise><i class="fa fa-circle lightGreen"></i>Budget</c:otherwise>
	</c:choose>
</p>

							</div>
						</div><!-- Row ends!!! -->
						</c:forEach>
						<nav>
						  <ul class="pagination pull-right">
							<li>
							  <a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							  </a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
							  <a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							  </a>
							</li>
						  </ul>
						</nav>
					</div>
					
					
				</div>
				<div class="col-md-3">
					<img src="static/img/rightbanner01.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner02.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner03.png" alt="..." class="img-responsive">
				</div>
			</div>	
		 </div>
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>
 




    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	<script src="static/js/lib/bootstrap-select.js"></script>
	<script src="static/js/views/locate.js"></script>


	<script>
		$( document ).ready(function() {
   			// $('[data-toggle="popover"]').popover();
			$('[data-toggle="tooltip"]').tooltip();
			
		});
	</script>
	
  </body>
</html>