<%@ include file="../head.jsp" %><!-- Main Page -->
        
    </head>
    
    <body>
        <div id="blockctnr">
        
        <sec:authorize access="isAuthenticated()">
					<c:set var="authenticated" scope="page">YES</c:set>
				</sec:authorize>
                <c:choose>
				  <c:when test= "${authenticated == 'YES'}">
					<c:if test="${pageContext.request.userPrincipal.name != null}">
					<div style="float:left;margin-top: 12px">
						<span style="margin-top:10px">User : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit();"> Logout</a></span>
					</div>
					</c:if>
				  </c:when>
				  <c:otherwise>
				  <div style="float:left;margin-top: 12px">
				    <a class="btn btn-primary" style="top:10px" data-target="#myModal" data-toggle="modal">Login</a>
					<!-- <a class="btn btn-warning"  href="registerPage.htm?new">New User</a> -->
				  </div>
				  </c:otherwise>
				</c:choose>
				<!-- Form to submit logout -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>
		<!-- End: Form to submit logout -->
        
           <div id="xbutton"><h2><a href="student_page.html">X</a></h2></div>
            <div id="blockin">
                <h1>Student's Result:</h1>
                    <div id="blockz">
                      <form:form id="submitPaper" method="POST" action="submitPaper.htm?qSet=${questionSet}" modelAttribute="data">
<!--Question Ans-->
					<c:forEach items="${resultList}" var="answerResult" varStatus="theCount"><!-- theCount.count for count value -->
						<ul>
							<li><p>Q${theCount.count}: ${answerResult} mark</p></li><br>
						</ul>
                    </c:forEach>
                    <br>
                    <p>Total Marks: <b>${totalMarks}</b> out of 5.</p>
                       <div style="float:right;margin-top: 100px">
                       	<a href="stud.htm">Go back to home page</a>
                       </div>
                       <div id="submitAns" class="hidden">
                       	<input type="submit" value="SUBMIT" />
                       </div>
                     </form:form>
                        
                    </div>
            </div>
        </div>
        <div id="footer">
            <div id="fotl"><h6>Copyright at school</h6></div>
            <div id="fotr"><h6>info@school.com</h6></div>
        </div>
        
        
    
    </body>
    
    
        
</html>