<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Landing page ..</title>
</head>
<body>

This is final Landing page: 

The Param value from URL is: ${testParam}

The message value from bean is: ${msgFromBean}

Data From DB:
<table id="userDetailTable" class="table table-condensed table-striped">
            <thead>
              <tr>
                <th id="userId"><spring:message code="USER_DETAIL.id"/></th>
                <th id="lastName"><spring:message code="USER_DETAIL.last_name"/></th>
                <th id="firstName"><spring:message code="USER_DETAIL.first_name"/></th>
                <th id="address"><spring:message code="USER_DETAIL.address"/></th>
                <th id="city"><spring:message code="USER_DETAIL.city"/></th>
              </tr>
			</thead>
			<tbody>
				<c:forEach items="${userBeanList}" var="userList">
					<tr>
						<td id="userIdVal">
						<a href="user_details.html?userId=${userList.id}" class="filter_link" id="${userList.id}">${userList.id}</a>
						</td>
						<td id="lastNameVal">${userList.lastName}</td>
						<td id="firstNameVal">${userList.firstName}</td>
						<td id="addressVal">${userList.address}</td>
						<td id="cityVal">${userList.city}</td>
					</tr>
				</c:forEach>
            </tbody>
          </table>

</body>
</html>