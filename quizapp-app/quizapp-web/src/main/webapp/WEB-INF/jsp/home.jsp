<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<body>
	<h1>Title : ${title}</h1>
	<h1>Message : ${message}</h1>

<sec:authorize access="isAuthenticated()">
    Welcome!! <sec:authentication property="principal.username" /> 
</sec:authorize>

	<sec:authorize access="hasRole('ROLE_USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	
	<a href="login.htm">Login</a> / <a href="register.htm?new">Register</a> <br><br> 
	<a href="contact_page.htm">Contact Us</a> / <a href="feedback.htm">Feedback</a> <br><br>
	<a href="register.htm">Profile</a> / <a href="feedback.htm">Feedback</a><br><br>
	
	<a href="dataFrom.htm">Checkout</a>
	
	<form:form action="search.htm" method="post" id="searchForm" modelAttribute="carSearchBean">
		From: <form:input id="from" path="fromCity" /><br>
		Duration: <form:select path="duration" class="form-control input-sm" id="duration">
			<form:option value="">Select...</form:option>
			<c:forEach items="${carSearchBean.duration}" var="duration">
			<form:option value="${duration}">${duration}</form:option>
			</c:forEach>
		</form:select><br>
		Date: <form:input id="date" path="date" /><br>
		<input type="submit" value="Search" />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
	</form:form>
	
</body>
</html>