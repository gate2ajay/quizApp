<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends : Post your business</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">
	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="static/js/lib/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="static/js/lib/jquery.validate.min.js"></script>
	<script type="text/javascript" src="static/js/biz/biz.js"></script>
	<script type="text/javascript" src="static/js/ajaxCallBack.js"></script>
	<script type="text/javascript" src="static/js/scripts.js"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
	<script>
		var bizPostLimit = ${bizPostLimit};
	</script>
    
    <style>
		.main-headline-index{
			content:url("static/img/mainheadlineindex.png");
		}
	</style>
  </head>
  <body>

	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="static/img/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="..." class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		  
		 
		 <div class="clearfix"></div>
		 <%@ include file="../headNav.jsp" %>
		 

		 <div class="container maincontent">
		 	<div class="row">
		 		<div class="col-md-9 well">
				<h1>Post your business:</h1>
	
	<div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
	</div>
	<!-- Using Layoutit -->
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<%-- <form class="form-horizontal" id="bannerForm" action="file.htm?${_csrf.parameterName}=${_csrf.token}" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							 
							<label class="control-label col-md-4" for="posterImage">
								Business banner
							</label>
							<div class="col-md-6"> 
							<input type="file" name="file" id="bannerImage" />
							<p class="help-block">
								Please upload image below 5MB and of type *.jpg and *.png. 
							</p>
							</div>
						</div>
						<div class="form-group">
							 
							<label class="control-label col-md-4" for="thumbImage">
								Thumbnail
							</label>
							<div class="col-md-6"> 
							<input type="file" name="file" id="thumbImage" />
							<p class="help-block">
								Please upload image below 50KB and of type *.jpg and *.png.
							</p>
							</div>
						</div>
						<input type="submit" Value="Submit" />
					</form> --%>
					<%-- <form class="form-horizontal" id="thumbForm" action="thumb.htm" method="POST" enctype="multipart/form-data">
						
					</form> --%>
					<form:form class="form-horizontal" id="postBiz" action="postBiz.htm?${_csrf.parameterName}=${_csrf.token}" method="POST" modelAttribute="bizBean" enctype="multipart/form-data">
						<div class="form-group">
						    <label class="control-label col-md-4" for="name">Name:</label>
						    <div class="col-md-6">
						      <form:input type="text" path="bizName" class="form-control" id="name" placeholder="Please enter name of business" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Nature of business:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="bizType" class="form-control" id="type" placeholder="Please enter your nature of business" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Area:</label><!-- //Implement Select box -->
						    <div class="col-md-6"> 
						      <form:input type="text" path="area" class="form-control" id="area" placeholder="Please enter business location" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Registered email:</label>
						    <div class="col-md-6"> 
						      <form:input type="email" path="registeredEmail" class="form-control" id="email" placeholder="Email" />
						  	</div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Speciality:</label>
						    <div class="col-md-6"> 
						      <form:textarea id="speciality" path="speciality" class="form-control" rows="5"></form:textarea>
						    </div>
						  </div>
						<!-- Image Place holder -->
						<div class="form-group">
							 
							<label class="control-label col-md-4" for="posterImage">
								Business banner
							</label>
							<div class="col-md-6"> 
							<input type="file" name="file" id="bannerImage" />
							<p class="help-block">
								Please upload image below 5MB and of type *.jpg and *.png. 
							</p>
							</div>
						</div>
						<div class="form-group">
							 
							<label class="control-label col-md-4" for="thumbImage">
								Thumbnail
							</label>
							<div class="col-md-6"> 
							<input type="file" name="file" id="thumbImage" />
							<p class="help-block">
								Please upload image below 50KB and of type *.jpg and *.png.
							</p>
							</div>
						</div>
						<!-- Image Placeholder Ends -->
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Description:</label>
						    <div class="col-md-6"> 
						      <form:textarea path="description" class="form-control" rows="5" id="desc"></form:textarea>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Full address:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="fullAddress" class="form-control" id="address" placeholder="Please enter address" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Mobile:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="mobileNumber" class="form-control" id="mobile" placeholder="Enter 10 digit number without country code" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Work Number:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="workNumber" class="form-control" id="type" placeholder="022-12345678" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Website:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="website" class="form-control" id="website" placeholder="example.com" />
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Map location:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" path="mapLocation" class="form-control" id="website" placeholder="Please enter map co-ordinates. eg: 19.0472871,72.8551903" />
						    </div>
						  </div>
						  <div class="form-group">
					      <div class="col-md-offset-4 col-md-10">
					        <input type="submit" value="Submit" class="submit"></input>
					        <input type="reset" value="Reset" class="reset"></input>
					      </div>
					    </div>
					</form:form>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>
	</div>

	<sec:authorize access="hasRole('USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	</div>
				<div class="col-md-3">
					<img src="static/img/rightbanner01.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner02.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner03.png" alt="..." class="img-responsive">
				</div>
			</div>	
		 </div>
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	
	
  </body>
</html>