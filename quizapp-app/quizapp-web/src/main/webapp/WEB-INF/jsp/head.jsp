<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
	<meta name="_csrf" content="${_csrf.token}"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Q & A :: ${title}</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">

	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="static/js/lib/jquery.validate.min.js"></script>
	<script type="text/javascript" src="static/js/user/register.js"></script>
	<script type="text/javascript" src="static/js/ajaxCallBack.js"></script>
	<script type="text/javascript" src="static/js/scripts.js"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
    <script src="static/js/lib/bootstrap.min.js"></script>
	<script src="static/js/lib/bootstrap-select.js"></script>
    <script src="static/js/appScripts.js"></script>
    <script src="static/js/ajaxCallBack.js"></script>
    <script src="static/js/scripts.js"></script>
    
    
    <script>
		var successVar = '${successVar}';
    </script>