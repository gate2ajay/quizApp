<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<%-- <link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.dataTables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/styles.css" />" /> --%>
	<script type="text/javascript" src="<c:url value="/js/lib/jquery-1.10.2.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/lib/jquery.validate.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/user/register.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/ajaxCallBack.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
	
	<script>
	var accountPresent = ${accountPresent};
	</script>
</head>
<body>
	<h1>Title : ${title}</h1>
	<h1>Message : ${message}</h1>
	
	<div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
	</div>
	<form:form id="registerUser" action="registerUser.htm" method="post" modelAttribute="userBean">
			<label for="username">Username *<br/><small>(5-25 alphanumeric characters)</small></label></td><td><form:input type="text" path="username" name="username" id="username" size="20" maxlength="25" ></form:input><br />
			<label for="password">Password *<br/><small>(5-25 alphanumeric characters)</small></label></td><td><form:input type="password" path="password" name="password" id="password" size="20" maxlength="25" ></form:input><br />
			<label for="password2">Repeat password *</label><input type="password" name="password2" id="password2" size="20" maxlength="25" ></input><br />
			<label for="name">First name *</label><form:input type="text" name="name" path="firstName" id="firstName" size="30" maxlength="100" ></form:input><br />
			<label for="name">Last name *</label><form:input type="text" name="lastName" path="lastName" id="lastName" size="30" maxlength="100" ></form:input><br />
			<label for="email">Email *</label><form:input type="text" name="email" path="email" id="email" size="30" maxlength="100"></form:input><br />
			<label for="address">Address *</label><form:textarea name="address" path="address" id="address" cols="50" rows="4"></form:textarea><br />
			<label for="email">Cellphone number *<br/><small>(only numbers)</small></label><form:input type="text" name="phone" path="mobile" id="phone" size="14" maxlength="20"></form:input><br />
			Newsletter<label><form:checkbox name="newsletter" path="newsLetter" id="newsletter"></form:checkbox> Subscribe to our newsletter</label><br />
			<br />
			<input type="submit" value="Register" class="submit"></input>

	</form:form>

	<sec:authorize access="hasRole('ROLE_USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	
</body>
</html>