<%@ include file="../head.jsp" %><!-- Main Page -->
        
    </head>
    
    <body>
        <div id="blockctnr" style="height:1000px;">
        
        <sec:authorize access="isAuthenticated()">
					<c:set var="authenticated" scope="page">YES</c:set>
				</sec:authorize>
                <c:choose>
				  <c:when test= "${authenticated == 'YES'}">
					<c:if test="${pageContext.request.userPrincipal.name != null}">
					<div style="float:left;margin-top: 12px">
						<span style="margin-top:10px">User : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit();"> Logout</a></span>
					</div>
					</c:if>
				  </c:when>
				  <c:otherwise>
				  <div style="float:left;margin-top: 12px">
				    <a class="btn btn-primary" style="top:10px" data-target="#myModal" data-toggle="modal">Login</a>
					<!-- <a class="btn btn-warning"  href="registerPage.htm?new">New User</a> -->
				  </div>
				  </c:otherwise>
				</c:choose>
				<!-- Form to submit logout -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>
		<!-- End: Form to submit logout -->
        
           <div id="xbutton"><h2><a href="student_page.html">X</a></h2></div>
            <div id="blockin" style="float:left;margin-left: 12px">
            <!-- Custom Code -->
                <h1>Register New User:</h1>
                    <div id="blockz">
	<div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
	</div>

	<form:form class="form-horizontal" id="registerUser" action="registerUser.htm" method="post" modelAttribute="userBean">
						<div class="form-group">
						    <label class="control-label col-md-4" for="name">Username *:</label>
						    <div class="col-md-6">
						      <form:input type="text" path="username" name="username" id="username" size="30" maxlength="25" ></form:input>
						      <p class="help-block">
								(5-25 alphanumeric characters) 
							</p>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="password">Password *:</label>
						    <div class="col-md-6"> 
						      <form:input type="password" path="password" name="password" id="passwordMain" size="30" maxlength="25" ></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="repeatPass">Repeat password *:</label>
						    <div class="col-md-6"> 
						      <input type="password" name="repeatPass" id="repeatPass" size="30" maxlength="25" ></input>
						    </div>
						  </div>
					<div class="form-group">
						    <label class="control-label col-md-4" for="name">Full Name *:</label>
						    <div class="col-md-6">
						      <form:input type="text" name="fullName" path="fullName" id="fullName" size="30" maxlength="100" ></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Email *:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" name="email" path="email" id="email" size="30" maxlength="100"></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Mobile:</label>
						    <div class="col-md-6"> 
						      +91<form:input type="text" name="phone" path="mobile" id="phone" size="20" maxlength="20"></form:input>
						      <p class="help-block">
								(10 digit mobile numbers)
							  </p>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="name">Address:</label>
						    <div class="col-md-6">
						      <form:textarea name="address" path="address" id="address" cols="50" rows="4"></form:textarea>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">City:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" name="city" path="city" id="city" size="30" maxlength="100"></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="name">Newsletter :</label>
						    <div class="col-md-6">
						      <label><form:checkbox name="newsletter" path="newsLetter" id="newsletter"></form:checkbox> Subscribe to our newsletter</label>
						    </div>
						  </div>
						  <div class="form-group">
					      <div class="col-md-offset-4 col-md-10">
					      	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					        <input type="submit" value="Register" class="submit"></input>
					        <input type="reset" value="Reset" class="reset"></input>
					      </div>
					    </div>
	</form:form>

	<sec:authorize access="hasRole('USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
                        
                    </div>
                    
                    <!-- Custom Code Ends-->
            </div>
        </div>
        <div id="footer">
            <div id="fotl"><h6>Copyright at school</h6></div>
            <div id="fotr"><h6>info@school.com</h6></div>
        </div>
        <!-- Common Login Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Login </h4>
		      </div>
		      <div class="modal-body">
		      	<c:url var="loginUrl" value="/login" />
			  	<form name="loginForm" action="${loginUrl}" method="POST">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Username</label>
				    <input type="text" name="username" class="form-control" id="username" placeholder="Username">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
				  </div>
				  <div class="form-group">
				    <label for="rememberMe">Remember Me</label>
				    <input type="checkbox" id="remember-me" name="remember-me"/>
				  </div>
				  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				  <button type="reset" class="btn btn-warning">Reset</button> 
				  <button type="submit" class="btn btn-primary">Submit</button><!-- Once submitted successfully!! data-dismiss="modal" -->
		
				</form>
		      </div>
		     
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div>
        
        
    
    </body>
    
    
        
</html>