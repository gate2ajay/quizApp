<%@page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
 	<!-- default header name is X-CSRF-TOKEN -->
 	<meta name="_csrf_header" content="${_csrf.headerName}"/>
 	<meta name="_csrf" content="${_csrf.token}"/>
	<%-- <link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.dataTables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/styles.css" />" /> --%>
	<script type="text/javascript" src="<c:url value="/js/lib/jquery-2.2.1.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/lib/jquery.validate.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/ajaxCallBack.js" />"></script>
	<script>
		var ctx = '${cp}';
	</script>
	<script type="text/javascript" src="<c:url value="/js/user/resetPass.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
	
</head>
<body>
	<h1>Title : ${title}</h1>
	<h1>Message : ${message}</h1>
	
	<div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
	</div>
	
	<c:choose>
    <c:when test="${submitted == true}">
       Request sent successfully.
    </c:when>
    <c:otherwise>
        <form:form id="forgotForm" action="set_pass.htm" method="post" modelAttribute="userBean">
        <form:input type="hidden" path="username" name="username" id="username" value=""></form:input>
			Enter new password:&nbsp;&nbsp;&nbsp;&nbsp;<form:input type="password" path="password" name="password" id="password" value="" placeholder="Enter Password"></form:input>
			Re-enter new password:&nbsp;&nbsp;&nbsp;<input type="password" name="password2" id="password2" value="" placeholder="Re-enter Password"></input>
			<input type="submit" value="Submit"></input>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" value="Reset"></input>
		</form:form>
    </c:otherwise>
</c:choose>

	<sec:authorize access="hasRole('ROLE_USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	
</body>
</html>