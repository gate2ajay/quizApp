<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
 	<meta name="_csrf" content="${_csrf.token}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends :: Locate</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">
	<link href="static/css/bootstrap-select.css" rel="stylesheet">

	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="static/js/lib/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="static/js/lib/jquery.validate.min.js"></script>
	<script type="text/javascript" src="static/js/ajaxCallBack.js"></script>
	<script>
		var ctx = '${cp}';
	</script>
	<script type="text/javascript" src="static/js/user/forgotPage.js"></script>
	<script type="text/javascript" src="static/js/scripts.js"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
  </head>
  <body>

	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="static/img/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="" class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		 <div class="clearfix"></div>
		 <%@ include file="../headNav.jsp" %> 
		 <div class="container">
		 	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="static/img/banner01.jpg" alt="..." class="img-responsive">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  
</div>
		 </div>
		 <div class="spacer"></div>
		 
		 <div class="container ">
		 	 <div class="row">
			 	
				<div class="col-md-9">
					<ol class="breadcrumb">
					  <li><a href="locate.htm">Locate</a></li>
					  <li><a href="#">Restaurant</a></li>
					  <li class="active">${bizBean.bizName}</li>
					</ol>
				</div>
				<div class="col-md-3">
					<!--<span class="pull-right"><a href=""><i class="fa fa-angle-double-left"></i> Back to Search</a></span>-->
				</div>
			 </div>
		 </div>		
		 
		 <div class="container maincontent">
		 	<div class="row">
		 		<div class="col-md-9 well">
		 			<h1>${title}</h1>
					<h3>${message}</h3>
					
					<div id="alertBox" class="alert alert-dismissable alert-success hidden">
						  <button type="button" class="close" id="closeAlert">&times;</button>
						  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
					</div>
					
					<c:choose>
				    <c:when test="${submitted == true}">
				       Request sent successfully.
				    </c:when>
				    <c:otherwise>
				        <form id="forgotForm" action="forgot.htm" method="post">
							E-mail:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="email" id="email" value="" placeholder="E-mail"></input>
							<input type="submit" value="Submit"></input>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="reset" value="Reset"></input>
						</form>
				    </c:otherwise>
				</c:choose>
		 		</div>
		 	</div>
		 </div>
		 
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>
 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	    <script src="static/js/lib/bootstrap-select.js"></script>


	<script>
		$( document ).ready(function() {
   			// $('[data-toggle="popover"]').popover();
			$('[data-toggle="tooltip"]').tooltip();
			
		});
	</script>
	
  </body>
</html>