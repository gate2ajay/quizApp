<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">
	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="static/js/lib/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="static/js/lib/jquery.validate.min.js"></script>
	<script type="text/javascript" src="static/js/user/register.js"></script>
	<script type="text/javascript" src="static/js/ajaxCallBack.js"></script>
	<script type="text/javascript" src="static/js/scripts.js"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
	<script>
		var accountPresent = ${accountPresent};
	</script>
    
    <style>
		.main-headline-index{
			content:url("static/img/mainheadlineindex.png");
		}
	</style>
  </head>
  <body>

	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="static/img/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="..." class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		  
		 
		 <div class="clearfix"></div>
		 <%@ include file="../headNav.jsp" %>
		 <div class="container">
		 	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="static/img/banner01.jpg" alt="..." class="img-responsive">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  
</div>
		 </div>

		 <div class="container maincontent">
		 	<div class="row">
		 		<div class="col-md-9 well">
				<h1>Register New User:</h1>
	
	<div id="alertBox" class="alert alert-dismissable alert-success hidden">
		  <button type="button" class="close" id="closeAlert">&times;</button>
		  <b><span id="alertType">Warning !</span></b> <span id="alertDetail">Alert Message goes here.</span>
	</div>

	<form:form class="form-horizontal" id="registerUser" action="registerUser.htm" method="post" modelAttribute="userBean">
						<div class="form-group">
						    <label class="control-label col-md-4" for="name">Username *:</label>
						    <div class="col-md-6">
						      <form:input type="text" path="username" name="username" id="username" size="30" maxlength="25" ></form:input>
						      <p class="help-block">
								(5-25 alphanumeric characters) 
							</p>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="password">Password *:</label>
						    <div class="col-md-6"> 
						      <form:input type="password" path="password" name="password" id="passwordMain" size="30" maxlength="25" ></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="repeatPass">Repeat password *:</label>
						    <div class="col-md-6"> 
						      <input type="password" name="repeatPass" id="repeatPass" size="30" maxlength="25" ></input>
						    </div>
						  </div>
					<div class="form-group">
						    <label class="control-label col-md-4" for="name">Full Name *:</label>
						    <div class="col-md-6">
						      <form:input type="text" name="fullName" path="fullName" id="fullName" size="30" maxlength="100" ></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Email *:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" name="email" path="email" id="email" size="30" maxlength="100"></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">Mobile:</label>
						    <div class="col-md-6"> 
						      +91<form:input type="text" name="phone" path="mobile" id="phone" size="20" maxlength="20"></form:input>
						      <p class="help-block">
								(10 digit mobile numbers)
							  </p>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="name">Address:</label>
						    <div class="col-md-6">
						      <form:textarea name="address" path="address" id="address" cols="50" rows="4"></form:textarea>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="type">City:</label>
						    <div class="col-md-6"> 
						      <form:input type="text" name="city" path="city" id="city" size="30" maxlength="100"></form:input>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-4" for="name">Newsletter :</label>
						    <div class="col-md-6">
						      <label><form:checkbox name="newsletter" path="newsLetter" id="newsletter"></form:checkbox> Subscribe to our newsletter</label>
						    </div>
						  </div>
						  <div class="form-group">
					      <div class="col-md-offset-4 col-md-10">
					      	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					        <input type="submit" value="Register" class="submit"></input>
					        <input type="reset" value="Reset" class="reset"></input>
					      </div>
					    </div>
	</form:form>

	<sec:authorize access="hasRole('USER')">
		<!-- For login user -->
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<h2>
				User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>
			</h2>
		</c:if>

	</sec:authorize>
	</div>
				<div class="col-md-3">
					<img src="static/img/rightbanner01.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner02.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner03.png" alt="..." class="img-responsive">
				</div>
			</div>	
		 </div>
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="static/js/lib/jquery-1.12.3.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	
	
  </body>
</html>