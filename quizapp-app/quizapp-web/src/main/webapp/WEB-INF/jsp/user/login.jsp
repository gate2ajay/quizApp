<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>m'friends</title>

    <!-- Bootstrap -->
    <link href="static/css/bootstrap.css" rel="stylesheet">
	<link  href="static/css/font-awesome.css" rel="stylesheet">
	<link href="static/css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="<c:url value="/js/lib/jquery-1.12.3.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/lib/jquery.validate.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/user/register.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/ajaxCallBack.js" />"></script>
	<script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>
	<script type="text/javascript" src="${cp}/dwr/engine.js"></script>
	<script type="text/javascript" src="${cp}/dwr/util.js"></script>
	<script>
		var accountPresent = ${accountPresent};
	</script>
    
    <style>
		.main-headline-index{
			content:url("static/img/mainheadlineindex.png");
		}
	</style>
  </head>
  <body onload='document.loginForm.username.focus();'>

	<div class="container-fluid">
		<div class="container header">
			 <div class="row">
				<div class="col-md-3"><img src="static/img/mfriendsLogo.jpg" alt="m'friends"></div>
				<div class="col-md-1">&nbsp;</div>
				<div class="col-md-8"> <img src="..." class="img-responsive" alt="Advertisement"></div>			
			 </div>
		 </div>
		  
		 
		 <div class="clearfix"></div>
		 <div class="container">
		 	<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  
				</div>
			
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					
					<li class="active"><a href="index.html">Home</a></li>
					<li><a href="newsfeed.htm">News Feed</a></li>
					<li><a href="locate.htm">Locate</a></li>
					<li><a href="aboutus.htm">About Us</a></li>
					<li><a href="contactUsPage.htm">Contact Us</a></li>
				  </ul>
				  
				  
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div> 


		 <div class="container maincontent">
		 	<div class="row">
		 		<div class="col-md-9 well"><!-- Main Dynamic Content area starts -->
					<!-- <h1>Spring Security Login Form (Database Authentication)</h1> -->

						<div id="login-box">
					
							<h2>Login with Username and Password</h2>
					
							<c:if test="${not empty error}">
								<div class="error">${error}</div>
							</c:if>
							<c:if test="${not empty message}">
								<div class="msg">${message}</div>
							</c:if>
					
							<c:url var="loginUrl" value="/login" />
							<form name="loginForm" action="${loginUrl}" method="POST">
					
							<table>
								<tr>
									<td>User:</td>
									<td><input type='text' name='username'></td>
								</tr>
								<tr>
									<td>Password:</td>
									<td><input type='password' name='password' /></td>
								</tr>
								<tr>
									<td><p><label for="remember-me">Remember Me</label></p></td>
									<td><input type="checkbox" id="remember-me" name="remember-me"/></td>
								</tr>
								<tr>
									<td colspan='2'><input name="submit" type="submit"
									  value="submit" /></td>
								</tr>
							  </table>
					
							  <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
					
							</form>
						</div>
				</div><!-- Ends!!! Main Dynamic Content area-->
				<div class="col-md-3">
					<img src="static/img/rightbanner01.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner02.png" alt="..." class="img-responsive">
					<img src="static/img/rightbanner03.png" alt="..." class="img-responsive">
				</div>
			</div>	
		 </div>
		 <div class="container footer">
		 	<div class="row">
			 	<div class="col-md-8">
					<p>&copy; m'friends . All rights reserved. </p>
				</div>
				<div class="col-md-4">
					
						<a href="javascript:;"><i class="fa fa-facebook-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-linkedin-square fa-3x"></i></a>
						<a href="javascript:;"><i class="fa fa-google-plus-square fa-3x"></i></a>						
						<a href="javascript:;"><i class="fa fa-twitter-square fa-3x"></i></a>
						
					
				</div>
			</div>	
		 </div>
	</div>
 


<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Login </h4>
      </div>
      <div class="modal-body">
        <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>

  	<button type="reset" class="btn btn-warning " data-dismiss="modal">Reset</button> <button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>

</form>
      </div>
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="static/js/lib/jquery-1.12.3.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="static/js/lib/bootstrap.min.js"></script>
	
	
  </body>
</html>