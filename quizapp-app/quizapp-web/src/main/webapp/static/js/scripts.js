/**
 * Please take care while updating Datatables Js and Datatables Min js as the message 
 * "Er zijn geen gegevens beschikbaar"(No data Available) is hard coded in these js libraries.,
 * 
 */

var mainScreenBundle = {
		       1:'Warning!',//WARN
		       2:'Info!',//INFO
		       3:'Success!',//SUCCESS
		       4:'1',//min_right
		       5:'3',//max_right
		       6:'alert alert-dismissable alert-danger',
		       7:'alert alert-dismissable alert-warning',
		       8:'alert alert-dismissable alert-success',
		       9:'First',//First text for Datatables
		       10:'Last',//Last text for Datatables
		       11:'Select...',
		       12:'Requesting the data took longer than anticipated and is therefore broken. Please try again.',
		       13:'Your request has not been answered in a timely manner and was terminated. Please try again. If the problem persists, contact the helpline.',
		    	   
};
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var ctx;
var loadedBy = '';
//Load jQuery First
//How frequently to check for session expiration in seconds
var tenMinFrequency = 600; // default 600 seconds - 10 mins
//How frequently to check for session expiration after 50 minutes in seconds
var oneMinFrequency = 60; // default 60 seconds - 1 min
//How many minutes the session is valid for in seconds
var sessionTimeout = 14400; //3600 - 1 hour, 14400 - 4 hours
//How many minutes before the warning prompt in Seconds
var warningBeforeTimeout = 600; //600 - 10 minutes
var tenMinutesCounter = 0;
var oneMinuteCounter = 0;
var oneSessionTimeoutId;
var tenSessionTimeoutId;
var layerDisplayed = false;
var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);

$(document).ready(function() {
	/**
	 * Code to Detect browser could be used some day. It uses backfix.min.js library already pushed to GIT :-)
	 */
	/*bajb_backdetect.OnBack = function() {
		var adhocIndexDownLoadStatus = $.urlParam('adhocIndexDownLoadStatus', document.referrer);
		if(adhocIndexDownLoadStatus != null && (adhocIndexDownLoadStatus == 'partialSuccess' || adhocIndexDownLoadStatus == 'success')) {
			//Here do not try to pull the excel file again.
			location.replace(document.referrer+'&backClicked=true');
		}
	};*/
	//ctx = $('#appContextPath').val();
	
	/*$.timeoutDialog({
		timeout: 1,
		countdown: 10,
		logout_redirect_url: window.location.protocol+'//'+window.location.host+ctx+'/session_expired.htm',
  		logout_url: window.location.protocol+'//'+window.location.host+ctx+'/session_expired.htm',
		title: 'Your session is about to expire!',
		message: 'You will be logged out in {0} minutes.',
		restart_on_yes: true,
	});*/

		initSessionExpiryCheck();
	
	/*$(document).ajaxStart($.blockUI({
        message: '<h4><img src="/images/loading.gif" /> Bezig met verwerken...</h4>',
        css: { border: '0px' },
        baseZ: 10000
    })).ajaxStop($.unblockUI);*/
	
	/*function errorHandler(msg, exc) {
		//unBlockUI();
		location.href = 'handle_error.html?error=DWR: '+msg;
	}
	dwr.engine.setErrorHandler(errorHandler);*/
	
	$('#closeAlert').click(function(){
		$('#alertBox').removeClass('visible').addClass('hidden');
	});
	
	jQuery.browser = {};
	(function () {
	    jQuery.browser.msie = false;
	    jQuery.browser.version = 0;
	    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
	        jQuery.browser.msie = true;
	        jQuery.browser.version = RegExp.$1;
	    }
	})();
	
	
	/**
     * Confirm window
     * 
     * @param {type} message
     * @param {type} callback
     * @returns {undefined}
     *//*
    BootstrapDialog.confirm = function(message, callback) {
        new BootstrapDialog({
            title: 'Uw sessie is bijna verlopen',
            message: message,
            closable: false,
            size: BootstrapDialog.SIZE_NORMAL,
            data: {
                'callback': callback
            },
            buttons: [{
                    label: 'Annuleren',
                    action: function(dialog) {
                        typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(false);
                        dialog.close();
                    }
                }, {
                    label: 'Ok',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                        typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(true);
                        dialog.close();
                    }
                }]
        }).open();
    };
    
    BootstrapDialog.confirmEditInitialTour = function(message, callback) {
        new BootstrapDialog({
            title: 'Bent u zeker?',
            message: message,
            closable: false,
            size: BootstrapDialog.SIZE_NORMAL,
            data: {
                'callback': callback
            },
            buttons: [{
                    label: 'Ja',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                        typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(true);
                        dialog.close();
                    }
                }, {
                    label: 'Nee',
                    action: function(dialog) {
                        typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(false);
                        dialog.close();
                    }
                }]
        }).open();
    };*/
	
    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    
    $.urlParam = function(name, prevUrl) {
        //var results; 
        if(prevUrl == undefined) {
        	//results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
        	 return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(window.location.href)||[,""])[1].replace(/\+/g, '%20'))||null;
        }else{
        	if(prevUrl != '') {
        		//results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(prevUrl);
        		 return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(prevUrl)||[,""])[1].replace(/\+/g, '%20'))||null;
        	}
        }
        //return results[1] || 0;
    };
    
    $(document).on("click", "a.exportExcelProcessing", function () {
    	loadingBlockUI(null, 'process');
        $.fileDownload($(this).prop('href'), {
        	successCallback: function(url) {
        		unBlockUI();
            },
            failCallback: function(responseHtml, url) {
            	//showAlert(mainScreenBundle.valueOf()[1], mainScreenBundle.valueOf()[14]);
            	unBlockUI();
            }
        });
        
        /*$.fileDownload($(this).prop('href'))
        .done(function () { 
        	unBlockUI();
			alert('File download a success!'); 
		})
        .fail(function () { 
        	unBlockUI();
			alert('File download failed!');  
        });*/
        
        
        return false; //this is critical to stop the click event which will trigger a normal file download!
    });
    
	
	$.fn.setCursorPosition = function(pos) {
		  this.each(function(index, elem) {
		    if (elem.setSelectionRange) {
		      elem.setSelectionRange(pos, pos);
		    } else if (elem.createTextRange) {
		      var range = elem.createTextRange();
		      range.collapse(true);
		      range.moveEnd('character', pos);
		      range.moveStart('character', pos);
		      range.select();
		    }
		  });
		  return this;
	};
	
	window.onbeforeunload = function(e) {
		return;
	};
	
	/*jQuery.i18n.properties({
		  name:'messages',
		  path:'static/',
		  mode:'map',
		  language:'nl',
		  callback: function() {
		    alert($.i18n.prop('OVERVIEW_INITIAL_TOURS_LABEL'));
		  }
	});*/
	
	if(isIE11) {
		//Search EAN screen
		$('#eanNumber').on( 'input', inputEntry.bind(this) ).on('blur', elemBlurred.bind(this)).on('focus', elemFocused.bind(this));
		//Overview Message EAN screen
		$('#fromDate').on( 'input', inputEntry.bind(this) );
		//Overview Initial Tour screen
		$('#ean').on( 'input', inputEntry.bind(this) ).on('blur', elemBlurred.bind(this)).on('focus', elemFocused.bind(this));
	}
	
	$("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
        //And to show use below javascript
        //onclick="$('.alert').show()"
    });
	
});

function callAjax(url, data, elem, header, token) {
	$.ajax({
        url: url,
        type: "POST",
        contentType: "application/json",
        /*dataType : "json",*/
        data: JSON.stringify(data),
        beforeSend : function(xhr) {
        	/*xhr.setRequestHeader("Accept", "application/json");*/
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(header, token);
        }
    }).done(function(callBackData) {
    	chooseCallBack(callBackData, elem);
	}).fail(function(res) {
		$(this).html("Error!");
	});
}

function inputEntry(elemObj) {
    var elemId = elemObj.target.id;
    var v = $('#'+elemId).val().length;
    if (v == 0 && $('#'+elemId).attr('placeholder')) {
		$('#'+elemId).removeAttr('placeholder');
	}else if (v > 0 && $('#'+elemId).val() == 'Eindigt op') {
		var rawData = $('#'+elemId).val();
		var alphabetsPresent = /[a-z ]/i.test(rawData);//Check for all case in-sensitive alphabets and spaces.
		if(alphabetsPresent) {
			$('#'+elemId).val('');
			$('#'+elemId).removeClass('ui-placeholder');
			$('#'+elemId).removeAttr("style");
		}
	}
}

function elemBlurred(obj) {
	var elemId;
	if(obj.target == undefined) {
		elemId = obj[0].id;
	}else{
		elemId = obj.target.id;
	}
	var v = $('#'+elemId).val().length;
	if (v == 0 && $('#'+elemId).val() == '') {
		$('#'+elemId).val('Eindigt op');
		$('#'+elemId).css('color', 'lightgrey');
		//$('#'+elemId).css('font-weight', 'bold');
		if(!$('#'+elemId).hasClass('ui-placeholder')) {
			$('#'+elemId).addClass('ui-placeholder');
		}
	}
}

function elemFocused(obj) {
	var elemId = obj.target.id;
	var v = $('#'+elemId).val().length;
	if (v > 0 && $('#'+elemId).val() == 'Eindigt op') {
		$('#'+elemId).val('');
		$('#'+elemId).removeClass('ui-placeholder');
		$('#'+elemId).removeAttr("style");
	}
}

function checkText(obj) {
	var elemId;
	if(obj.target == undefined) {
		elemId = obj[0].id;
	}else{
		elemId = obj.target.id;
	}
	var rawData = $('#'+elemId).val();
	 var alphabetsPresent = /[a-z ]/i.test(rawData);//Check for all case in-sensitive alphabets and spaces.
	 if(alphabetsPresent) {
		 $('#'+elemId).val('');
		 $('#'+elemId).removeClass('ui-placeholder');
		 $('#'+elemId).removeAttr("style");
	 }
}

/*function removeSession() {
	//location.href = 'invalidate_session.html';
}
*/

function isTextSelected(input){
	   var startPos = input.selectionStart;
	   var endPos = input.selectionEnd;
	   var doc = document.selection;

	   if(doc && doc.createRange().text.length != 0){
	      return true;
	   }else if (!doc && input.value.substring(startPos,endPos).length != 0){
	      return true;
	   }
	   return false;
	}

function getFinalMessage(message, argList) {
	if(message.indexOf('{0}') != -1) {
		for(var i = 0; i < argList.length; i++) {
			message = message.replace('{'+i+'}', argList[i]);
		}
	}
	return message;
}

function datatableWhiteSpaceFix(tableHTML, tableName) {
	if (jQuery.browser.msie && jQuery.browser.version === '9') {
		tableHTML = tableHTML.replace(/>\s+(?=<\/?(t|c)[hardfob])/gm,'>');
	}
	$('#'+tableName).html(tableHTML);
}

function loadingBlockUI(action, actionType) {
	if(actionType == 'process') {
		loadedBy = 'process';
	}
	if(action == 'detailInitialTour') {
		$.blockUI({
	        message: '<h4><img src="static/images/loading.gif" /> Bezig met verwerken...</h4>', 
	        css: { border: '0px' },
	        baseZ: 10000
	    });
	}else{
		$.blockUI({
	        message: '<h4><img src="static/images/loading.gif" /> Bezig met verwerken...</h4>', 
	        timeout: 60000,
	        onBlock: function() { 
	        	myLoadBlockTimeout = setTimeout(function () {
	        		 if(action == 'massMrod') {
	        			 unBlockUI();
	        			 $('#stillProcessingDiv').removeClass('hidden');
	        			 //showAlert(mainScreenBundle.valueOf()[2], mainScreenBundle.valueOf()[15]);
	        		 }
		    	 }, 20000);
	        },
	        css: { border: '0px' },
	        baseZ: 10000
	    });
	}
}

function unBlockUI(myTimeout) {
	clearTimeout(myTimeout);
	$.unblockUI({
		onUnblock: function() { 
			// Common code for after unblock
		} 
	});
	
	/*setTimeout(function() { 
        $.unblockUI({ 
            onUnblock: function(){ alert('onUnblock called'); }
        }); 
    }, 20000);*/
}

/*Convert date object to dd/mm/yyyy format */
function convertDateFormat(dateObj) {
	if(dateObj != null && dateObj != undefined) {
		var dateNum = dateObj.getDate();
		if(dateNum < 10) {
	    	dateNum = '0'+dateNum;
	    }
	    var monthNum = dateObj.getMonth()+1;
	    if(monthNum < 10) {
	    	monthNum = '0'+monthNum;
	    }
	    return dateNum+'/'+monthNum+'/'+dateObj.getFullYear();
	}else{
		return '';
	}
}

/*Convert date object to dd/mm/yyyy format */
function convertStringToDate(dateStr) {
var parts = dateStr.split("/");
var dt = new Date(parseInt(parts[2], 10),
                  parseInt(parts[1], 10) - 1,
                  parseInt(parts[0], 10));
return dt;
}

//JAVA Ajax using JSON
function ajaxAsyncRequest(reqURL) {
	// Creating a new XMLHttpRequest object
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest(); // for IE7+, Firefox, Chrome, Opera,
										// Safari
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); // for IE6, IE5
	}
	// Create a asynchronous GET request
	xmlhttp.open("GET", reqURL, true);

	// When readyState is 4 then get the server output
	xmlhttp.onreadystatechange = function() {
		alert(xmlhttp.readyState);
		if (xmlhttp.readyState == 4) {
			alert(xmlhttp.status);
			if (xmlhttp.status == 200) {
				document.getElementById("message").innerHTML = xmlhttp.responseText;
				// alert(xmlhttp.responseText);
			} else {
				alert('Something is wrong !!');
			}
		}
	};

	xmlhttp.send(null);
}

function clearAlert(){
	$('#alertBox').fadeOut('slow');
	clearAlerts();
}
function clearAlerts() {
	var alerts = document.querySelectorAll('*[id^="alertBox"]');
	var alertsLength = alerts.length;
	for(var i = 0; i < alertsLength; i++) {
		//$('#'+alerts[0].id).removeClass('visible').addClass('hidden');
		$('#'+alerts[i].id).fadeOut('slow');
	}
}
function showAlert(type, message, place) {
	var classType='alert alert-dismissable alert-danger';
	if(type == mainScreenBundle.valueOf()[1] || type=='error'){
		classType = 'alert alert-dismissable alert-danger';
	}
	if(type == mainScreenBundle.valueOf()[2]){
		classType = 'alert alert-dismissable alert-warning';
	}
	if(type == mainScreenBundle.valueOf()[3]){
		classType = 'alert alert-dismissable alert-success';
	}
	var alertId = 'alertBox';
	if(place =='mid'){
		alertId='alertBoxMid';
	}
	if(alertId=='alertBox') {
		$('#alertType').text(type);
		$('#alertDetail').html(message);
		$('#alertBox').removeClass();
		$('#alertBox').addClass(classType);
		$('#alertBox').fadeIn('slow');//delay(5000).fadeOut('slow')
		scrollToTop();
	}
	if(alertId=='alertBoxMid') {
		$('#alertTypeMid').text(type);
		$('#alertDetailMid').html(message);
		$('#alertBoxMid').removeClass();
		$('#alertBoxMid').addClass(classType);
		$('#alertBoxMid').fadeIn('slow');//delay(5000).fadeOut('slow')
	
	}
	
}

function isValidDate(dateValue) {
	//var rgexp = /(^(((0[1-9]|[12][0-8])[\/.](0[1-9]|1[012]))|((29|30|31)[\/.](0[13578]|1[02]))|((29|30)[\/.](0[4,6,9]|11)))[\/.](19|[2-9][0-9])\d\d$)|(^29[\/.]02[\/.](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;
	//var rgexp = /(^((0[13578]|1[02])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](18|19|20)[0-9]{2})|((0[469]|11)[\/.](0[1-9]|[12][0-9]|30)[\/.](18|19|20)[0-9]{2})|((02)[\/.](0[1-9]|1[0-9]|2[0-8])[\/.](18|19|20)[0-9]{2})|((02)[\/.]29[\/.](((18|19|20)(04|08|[2468][048]|[13579][26]))|2000))$)/;
	/*var rgexp = /^(0[1-9]|1\d|2[0-8]|29(?=-\d\d-(?!1[01345789]00|2[1235679]00)\d\d(?:[02468][048]|[13579][26]))|30(?!-02)|31(?=-0[13578]|-1[02]))-(0[1-9]|1[0-2])-([12]\d{3})(\s([01]\d|2[0-3]):([0-5]\d):([0-5]\d))?$/gm;
	var res = date.replace("/", "-");
	res = res.replace("/", "-");
	var isValidDate = rgexp.test(res);
	return isValidDate;*/
	var bits = dateValue.split('/');
	var y = bits[2], m  = bits[1], d = bits[0];
	// Assume not leap year by default (note zero index for Jan)
	var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
	// If evenly divisible by 4 and not evenly divisible by 100,
	// or is evenly divisible by 400, then a leap year
	if ( (!(y % 4) && y % 100) || !(y % 400)) {
	  daysInMonth[1] = 29;
	}
	return d <= daysInMonth[--m];
}

/**
 * Function to Check for empty Map object in JavaScript.
 * @param map
 * @returns {Boolean}
 */
function isEmptyObject(map) {
	for(var key in map) {
		if (map.hasOwnProperty(key)) {
			return false;
	    }
	}
	return true;
}

function scrollToTop() {
	window.scrollTo(0,0);
	//Below Code scrolls with ease to the top of screen
	/*var yScroll;
	if(document.documentElement && document.documentElement.clientWidth) {
	  yScroll = document.documentElement.scrollTop;
	} else if (window.innerWidth) {
	  yScroll = window.pageYOffset;
	}
	for(var i = 0; i <= yScroll; i+=10) {
	  (function(i) {
	    setTimeout(function() {
	      window.scrollBy(0, -i);
	    }, 10*i);
	  })(i);
	}*/
}

function sortByValue(obj) {
    var tuples = [];

    for (var key in obj) tuples.push([key, obj[key]]);

    tuples.sort(function(a, b) {
        a = a[1];
        b = b[1];

        return a < b ? -1 : (a > b ? 1 : 0);
    });

    return tuples;
    /*for (var i = 0; i < tuples.length; i++) {
        var key = tuples[i][0];
        var value = tuples[i][1];
        // do something with key and value
    }*/
}

/*
 * Add only Business Days, Currently implemented to skip only weekends.
 * But can be extented to include national holidays and holidays as well.  
 */
function addBusinessDays(weekDaysToAdd) {
  var curdate = new Date();
  var realDaysToAdd = 0;
  while (weekDaysToAdd > 0) {
    curdate.setDate(curdate.getDate()+1);
    realDaysToAdd++;
    //check if current day is business day
    if (noWeekendsOrHolidays(curdate)[0]) {
      weekDaysToAdd--;
    }
  }
  return realDaysToAdd;
}
function noWeekendsOrHolidays(date) {
  var noWeekend = noWeekends(date);
  if (noWeekend[0]) {
      //return nationalDays(date);
  } else {
      return noWeekend;
  }
  //Comment below code if nationDays(date) is uncommented
  return noWeekend;
}
//holidays
var natDays = [
[1, 1, 'nl'],
[12, 25, 'nl'],
[12, 26, 'nl']
];
function nationalDays(date) {
  for (var i = 0; i < natDays.length; i++) {
      if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
          return [false, natDays[i][2] + '_day'];
      }
  }
  return [true, ''];
}
/* Set as beforeShowDay function to prevent selection of weekends.
* @param  date  Date - the date to customise
* @return [boolean, string] - is this date selectable?, what is its CSS class?
*/
function noWeekends(date) {
	var day = date.getDay();
	return [(day > 0 && day < 6), ""];
};

function formatDate(dateObj) {
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1; //January is 0!

    var yyyy = dateObj.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    return dd+'/'+mm+'/'+yyyy;
}
//End Date







function initSessionExpiryCheck() {
	setSessionExpiryCheck();
	$(document).bind('keypress.session', function (ed, e) { sessKeyPressed(ed, e); });
}

function sessKeyPressed(ed, e) {
	tenMinutesCounter = 0;
	oneMinuteCounter = 0;
	window.clearInterval(oneSessionTimeoutId);
	window.clearInterval(tenSessionTimeoutId);
	setSessionExpiryCheck();
}

function setSessionExpiryCheck() {
	tenSessionTimeoutId = setInterval('checkForWarning()', convertSecondsToMilliSeconds(tenMinFrequency));
}

function checkForWarning() {
	tenMinutesCounter = tenMinutesCounter + 1;
	if(tenMinutesCounter >= 23 ) {
		window.clearInterval(tenSessionTimeoutId); 
		window.clearInterval(oneSessionTimeoutId);
		oneSessionTimeoutId = setInterval('checkSessionExpiry()', convertSecondsToMilliSeconds(oneMinFrequency));
	}
}

function checkSessionExpiry() {
	oneMinuteCounter = oneMinuteCounter + 1;
	if(!layerDisplayed) {
		if(oneMinuteCounter >= 5) {
			layerDisplayed = true;
			BootstrapDialog.confirm('Deze sessie wordt over 5 minuten afgesloten' +
					' \n Klik op OK om de sessie te verlengen.', function(result) {
				if(result) {
					layerDisplayed = false;
					tenMinutesCounter = 0;
					oneMinuteCounter = 0;
					window.clearInterval(oneSessionTimeoutId);
					window.clearInterval(tenSessionTimeoutId);
					setSessionExpiryCheck();
				}else{
					sessionLogOut();
				}
			});
		}
	}
	
	if(oneMinuteCounter >= 10) {
		window.clearInterval(oneSessionTimeoutId);
    	window.clearInterval(tenSessionTimeoutId);
		sessionLogOut();
	}
}

function sessionLogOut() {
    window.location.href = 'session_expired.htm';
}

function convertSecondsToMilliSeconds(seconds) {
	var milliSeconds = seconds * 1000;
	return milliSeconds;
}

/* Temporary client side memory implementation via Cookies */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function checkCookie(cname) {
    var cookieValue = getCookie(cname);
    if (cookieValue != '') {
        return true;
    } else {
        return false;
    }
}
function deleteCookie( cname ) {
	document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function replaceFromHtmlToEntities(text){
	if(text.indexOf('<')!= -1)
		text = text.replace(/</g, '&lt;');
	if(text.indexOf('>')!= -1)
		text = text.replace(/>/g, '&gt;');
	
	return text;
}

function replaceFromEntitiesToHtml(text){
	if(text.indexOf('&lt;')!= -1)
		text = text.replace(/&lt;/g, '<');
	if(text.indexOf('&gt;')!= -1)
		text = text.replace(/&gt;/g, '>');
	
	return text;
}

function getPosition(str, m, i) {
	
	return str.split(m, i).join(m).length;
	
	//return str.split(m, i).join(m).length;
	} 

/** Function count the occurrences of substring in a string;
 * @param {String} string   Required. The string;
 * @param {String} subString    Required. The string to search for;
 * @param {Boolean} allowOverlapping    Optional. Default: false;
 * @author Vitim.us http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string/7924240#7924240
 */
function countOccurance(string, subString) {

	string += ""; 
	subString += "";
	if (subString.length<=0){
		return string.length+1;
	}
	var n = 0, pos = 0;
	var step = 1;
	while (true) {
		pos=string.indexOf(subString,pos);
		if(pos >= 0) {++n; pos+=step;
		} else {
			break;
		}
	}
	return n;
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

var stringify_aoData = function (aoData) {
    var o = {};
    var modifiers = ['mDataProp_', 'sSearch_', 'iSortCol_', 'bSortable_', 'bRegex_', 'bSearchable_', 'sSortDir_'];
    jQuery.each(aoData, function(idx,obj) {
        if (obj.name) {
            for (var i=0; i < modifiers.length; i++) {
                if (obj.name.substring(0, modifiers[i].length) == modifiers[i]) {
                    var index = parseInt(obj.name.substring(modifiers[i].length));
                    var key = 'a' + modifiers[i].substring(0, modifiers[i].length-1);
                    if (!o[key]) {
                        o[key] = [];
                    }
                    //console.log('index=' + index);
                    o[key][index] = obj.value;
                    //console.log(key + ".push(" + obj.value + ")");
                    return;
                }
            }
            //console.log(obj.name+"=" + obj.value);
            o[obj.name] = obj.value;
        }
        else {
            o[idx] = obj;
        }
    });
    return JSON.stringify(o);
};