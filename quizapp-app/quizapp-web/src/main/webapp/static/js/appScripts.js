$(function() {
    $("#prev").click(function(e) {
      e.preventDefault(); // if desired...
      // other methods to call...
    });
    
    $("#next").click(function(e) {
        e.preventDefault(); // if desired...
        // other methods to call...
      });
  });

function nextClicked(count) {
	var xCount = parseInt(count);
	if(xCount == 5) {
		$('#submitAns').removeClass('hidden').addClass('visible');
		
	}else{
		$('#question'+count).addClass('hidden').removeClass('visible');
		$('#question'+(xCount+1)).removeClass('hidden').addClass('visible');
	}
}

function prevClicked(count) {
	var xCount = parseInt(count);
	if(xCount == 1) {
		return false;
	}
	$('#question'+count).addClass('hidden').removeClass('visible');
	$('#question'+(xCount-1)).removeClass('hidden').addClass('visible');
}