$(document).ready(function() {

	$('#forgotForm').submit(function(e) {
	    var data = new Object();
	    $.each(this.elements, function(i, v) {
	    	var input = $(v);
			data[input.attr("name")] = input.val();
			delete data["undefined"];
			delete data[undefined];
	    });
	    
	    var token = $("meta[name='_csrf']").attr("content");
	    var header = $("meta[name='_csrf_header']").attr("content");
	    var url = $('#forgotForm').attr('action');
	    callAjax(url, data, 'forgot', header, token);
	    
	    /*$.ajax({
	    	cache : false,
	        url : "/spring-web/forgot.htm",
	        type: "POST",
	        contentType : "application/json",
	        data : JSON.stringify(data),
	    }).done(function(callBackData) {
				chooseCallBack(callBackData, 'forgot');
		}).fail(function(res) {
			$(this).html("Error!");
		});*/
	    
	    e.preventDefault();
	    
	});

});