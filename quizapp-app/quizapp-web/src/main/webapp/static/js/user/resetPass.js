$(document).ready(function() {
	$("#resetPassForm").validate({
		rules: {
			  password: {
				  required : true,
				  minlength: 4
			  },
			  repeatPass: {
				  required : true,
				  minlength: 4,
				  equalTo: '#passwordMain',
			  }
		   },
		   messages : {
			   password: {
				   required : 'Please enter a valid password',
				   minlength : 'Password should be minimum 5 characters long.',
			   },
			   repeatPass: {
				   required : 'Please enter a confirm password.',
				   minlength : 'Confirm password should be minimum 5 characters long.',
				   equalTo: 'Confirm password should be same as above.',
			   }
           },
	});

});