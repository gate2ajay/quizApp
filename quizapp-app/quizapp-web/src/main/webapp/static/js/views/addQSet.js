/*var screenBundleEan = {
		       1:'Account already present. Click <a href="forgot_page.htm">here</a> to reset username/password.',
		       
};*/
$('.alert').show();

$(function() {
	$('#noOfQuestion')
	  .focusout(function() {
		  var noOfQuestions = $('#noOfQuestion').val();
		  displayQuestions(noOfQuestions);
	  })
	  .blur(function() {
		  var noOfQuestions = $('#noOfQuestion').val();
		  displayQuestions(noOfQuestions);
	  });
	
	jQuery(document).on('blur', 'textarea[name*=\'answer\']', function () {
	    var value = jQuery(this)[0].id;
	    var answer = $('#'+value).val();
	    var questionNo = value.substring(6);
	    var dataObj = {};
	    dataObj['answer'] = answer;
	    var token = $("meta[name='_csrf']").attr("content");
	    var header = $("meta[name='_csrf_header']").attr("content");
	    callAjax('getTags.htm', dataObj, 'parse'+questionNo, header, token);
	});

	
	var displayQuestions = function(noOfQuestions){
		if(noOfQuestions != null && noOfQuestions != '') {
			  var dynamicQuestions = '';
			  for(var i=0; i < noOfQuestions; i++) {
				  dynamicQuestions = dynamicQuestions+'<div id="questionDiv'+(i+1)+'"> <!-- Main question div -->\
					  <div class="row form-group"> \
	          		<div class="col-md-1 text-right"><label for="qNo">Q'+(i+1)+':</label></div> \
	          	</div> \
              <div class="row form-group"> \
                  <div class="col-md-8 text-left"><textarea class="form-control textarea" type="text" name="question" id="question'+(i+1)+'" value="" rows="3"></textarea></div> \
                  <div class="col-md-2 text-right"><label for="wt">Weight: </label></div> \
                  <div class="col-md-2"> \
                      <input class="form-control" type="text" name="weight" id="weight'+(i+1)+'" size="5" maxlength="5" value="" ></input> \
                  </div> \
              </div> \
              <div class="row form-group"> \
	              <div class="col-md-2 text-right"><label for="wt">Answer: </label></div> \
	              <div class="col-md-12"> \
	                  <div class="col-md-12 text-left"><textarea class="form-control textarea answer" type="text" name="answer" id="answer'+(i+1)+'" value="" rows="3"></textarea></div> \
	              </div> \
	          </div> \
              <div id="qTags'+(i+1)+'" class=""> \
              </div> \
              </div><!-- Main question div ends-->';

			  }
			  $('#dynamic').html(dynamicQuestions);
		  }
	};
	
	var displayTags = function(noOfWords, wordsList){
		if(noOfWords != null && noOfWords!= '' && noOfWords != 0) {
			var dynamicTags = '<div class="row">';
			  for(var i=0; i < noOfWords; i++) {
				  dynamicTags = dynamicTags+'<div class="col-xs-12 col-sm-12 col-md-4"> \
		        	<div class="col-md-8 text-right"><label for="word">valueVerybigwordHere </label></div> \
		            <div class="col-md-4"> \
		                <input class="form-control" type="text" name="weight" id="weight" value="" ></input> \
		            </div> \
		        </div>';
				if(i == (noOfWords-1)) {
					dynamicTags = dynamicTags+'</div>';
				}
			  }
			  $('#qTags').html(dynamicQuestions);
		}
		
	};

	
	
});


function displayTags(noOfWords, wordsList, questionNo){
	if(noOfWords != null && noOfWords!= '' && noOfWords != 0) {
		var dynamicTags = '<div class="row">';
		  for(var i=0; i < noOfWords; i++) {
			  dynamicTags = dynamicTags+'<div class="col-xs-12 col-sm-12 col-md-4"> \
	        	<div class="col-md-8 text-right"><label for="word">'+wordsList[i]+' </label></div> \
	            <div class="col-md-4"> \
	                <input class="form-control" type="text" name="weight" id="weight" value="" ></input> \
	            </div> \
	        </div>';
			if(i == (noOfWords-1)) {
				dynamicTags = dynamicTags+'</div>';
			}
		  }
		  $('#qTags'+questionNo).html(dynamicTags);
	}
	
}