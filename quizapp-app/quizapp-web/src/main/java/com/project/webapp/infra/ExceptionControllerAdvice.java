/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: ExceptionControllerAdvice.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Apr 9, 2014
 */

package com.project.webapp.infra;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.project.webapp.bean.UserBean;
import com.project.webapp.controller.AppController;
import com.project.webapp.controller.ControllerLiterals;

/**
 * ExceptionControllerAdvice is a global exception controller for the application
 * 
 */

@ControllerAdvice
public class ExceptionControllerAdvice
    extends AppController {

    /**
     * Variable to hold the object of Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(ExceptionControllerAdvice.class);

    @SuppressWarnings("unchecked")
    @ExceptionHandler(Exception.class)
    public ModelAndView exception(final Exception e, HttpSession session) {

        UserBean userBean = (UserBean) session.getAttribute("userDetail");

        String viewName = "errorPage";
        /**
         * Creating Object of ModelAndView class and passing exception_error param
         * 
         */
        /*
         * if (e instanceof PageBlockException) { viewName = "blockPage"; }
         */
        ModelAndView modelAndViewObj = new ModelAndView(viewName);

        /**
         * Changes for link ordering should also be done in Excection handling page as sessions are
         * not accessible in error/block JSP pages. Hence, session data is stuffed into model
         * object.
         */
        if (userBean != null) {
            modelAndViewObj.addObject(ControllerLiterals.RIGHT_ID.getName(), 3);
        }
        /**
         * Inserting the key-value pair to ModelAndView object for Exception Name
         * 
         */
        modelAndViewObj.addObject("name", e.getClass().getSimpleName());

        /**
         * Inserting the key-value pair to ModelAndView object for Exception Message
         * 
         */
        modelAndViewObj.addObject("message", e.getMessage());

        /**
         * For testing
         * 
         */
        LOGGER.info("Inside Custome Exception Handler class--->" + e.getMessage());

        /**
         * return ModelAndView Object
         * 
         */
        return modelAndViewObj;
    }
}
