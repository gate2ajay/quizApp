package com.project.webapp.dao;

import java.util.List;

import com.project.webapp.entity.QA;

public interface DBService {

	public int getQuestionSetId(String questionSet);

	public List<QA> getQuestionList(int qSetId);
}
