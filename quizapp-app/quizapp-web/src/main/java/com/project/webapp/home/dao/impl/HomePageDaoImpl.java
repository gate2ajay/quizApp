package com.project.webapp.home.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.project.webapp.dao.impl.AbstractDaoImpl;
import com.project.webapp.entity.Message;
import com.project.webapp.entity.User;
import com.project.webapp.entity.UserRole;
import com.project.webapp.home.dao.HomePageDao;

@Repository("homePageDao")
public class HomePageDaoImpl
    extends AbstractDaoImpl<Object, String>
    implements HomePageDao {

    /**
     * Variable to hold the object of Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(HomePageDaoImpl.class);

    /**
     * This constructor calls the super class constructor.
     */
    public HomePageDaoImpl() {

        super();
    }

    @Override
    public User getUserDetails(String username) {

        Query query = getCurrentSession().createQuery("from User where username=:username");
        query.setParameter("username", username);
        LOGGER.debug("Fetching User Details");
        User userEntity = null;
        if (!query.list().isEmpty()) {
            userEntity = (User) query.list().get(0);
        }
        return userEntity;
    }

    @Override
    public User getUserDetailsFromEmail(String email) {

        Query query = getCurrentSession().createQuery("from User where email=:email");
        query.setParameter("email", email);
        LOGGER.debug("Fetching User Details");
        User userEntity = null;
        if (!query.list().isEmpty()) {
            userEntity = (User) query.list().get(0);
        }
        return userEntity;
    }

    public List<Message> getMessagesForUser(String username) {

        Query query = getCurrentSession().createQuery("from Message where username=:username");
        query.setParameter("username", username);
        LOGGER.debug("Fetching User Details");
        List messagesList = query.list();
        return messagesList;
    }

    public boolean saveUser(User userEntity) {

        saveOrUpdate(userEntity);
        if (userEntity.getId() != 0L) {
            UserRole roleUserEntity = new UserRole();
            roleUserEntity.setRoleId(2);
            roleUserEntity.setUserId((int) userEntity.getId());
            saveOrUpdate(roleUserEntity);
            return true;
        }

        return false;
    }

    public void saveMessage(Message messageEntity) {

        saveOrUpdate(messageEntity);
    }

    public boolean checkEmail(String email) {

        Query query = getCurrentSession().createQuery("from User where email=:email");
        query.setParameter("email", email);
        LOGGER.debug("User Email Check");
        if (query.list().isEmpty()) {
            return false;
        }
        return true;
    }
}
