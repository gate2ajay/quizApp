package com.project.webapp.bean;

import org.springframework.stereotype.Component;

@Component
public class UserBean {

    private long id;

    private String fullName;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String email;

    private String address;

    private String mobile;

    private String city;

    private boolean newsLetter;

    private boolean enabled;

    private String resetCode;

    private boolean resetStatus;

    @Override
    public String toString() {

        return "UserBean [email=" + email + ", Full Name=" + fullName + "]";
    }

    /**
     * @return the id
     */
    public long getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {

        this.id = id;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {

        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {

        this.fullName = fullName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {

        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {

        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    /**
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {

        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {

        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {

        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {

        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {

        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {

        this.address = address;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {

        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {

        this.mobile = mobile;
    }

    /**
     * @return the city
     */
    public String getCity() {

        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {

        this.city = city;
    }

    /**
     * @return the newsLetter
     */
    public boolean isNewsLetter() {

        return newsLetter;
    }

    /**
     * @param newsLetter the newsLetter to set
     */
    public void setNewsLetter(boolean newsLetter) {

        this.newsLetter = newsLetter;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {

        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {

        this.enabled = enabled;
    }

    /**
     * @return the resetCode
     */
    public String getResetCode() {

        return resetCode;
    }

    /**
     * @param resetCode the resetCode to set
     */
    public void setResetCode(String resetCode) {

        this.resetCode = resetCode;
    }

    /**
     * @return the resetStatus
     */
    public boolean isResetStatus() {

        return resetStatus;
    }

    /**
     * @param resetStatus the resetStatus to set
     */
    public void setResetStatus(boolean resetStatus) {

        this.resetStatus = resetStatus;
    }

}
