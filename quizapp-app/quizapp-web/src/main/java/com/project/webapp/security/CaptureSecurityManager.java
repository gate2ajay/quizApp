/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: CaptureSecurityManager.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.security;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.project.webapp.bean.UserBean;

@Component("securityManager")
@Transactional
public class CaptureSecurityManager
    implements ApplicationContextAware {

    /**
     * Variable to hold the object of Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(CaptureSecurityManager.class);

    @Autowired
    private ApplicationContext applicationContext;

    public String getUserName(HttpServletRequest request) {

        String userName = "";
        Principal principal = request.getUserPrincipal();

        if (principal != null) {
            userName = principal.getName();
        } else {
            userName = "Tested";
        }
        return userName;
    }

    public void initializeUserSession(ServletRequestAttributes attr, String screenId) {

        String firstName = "", lastName = "", username = "";

        HttpServletRequest request = attr.getRequest();
        HttpSession session = request.getSession(false);
        if (session == null) {
            LOGGER.info("SECURITY::Session Not Available, session is null");
            LOGGER.info("SECURITY::Creating New Session");
            session = request.getSession();
            LOGGER.info("SECURITY::Created New Session and Session ID is: " + request.getSession().getId());
        } else {
            LOGGER.info("SECURITY::Session Available " + request.getSession().getId());
        }

        username = getUserName(request);

        UserBean userBean = new UserBean();

        userBean.setFirstName(firstName);
        userBean.setLastName(lastName);
        LOGGER.info("First Name is: " + firstName);
        LOGGER.info("Last Name is: " + lastName);

        session.setAttribute("userDetail", userBean);
        session.setAttribute("rightId", 3);

    }

    public ApplicationContext getApplicationContext() {

        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {

        this.applicationContext = applicationContext;
    }

}
