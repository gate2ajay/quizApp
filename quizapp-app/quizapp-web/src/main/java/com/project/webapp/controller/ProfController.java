package com.project.webapp.controller;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.webapp.bean.JsonData;
import com.project.webapp.utils.Utilities;

@RestController
public class ProfController {

	@RequestMapping(value = "/getTags", method = RequestMethod.POST)
	@ResponseBody
    public String getParsedString(@RequestBody JsonData data) {
        //model.addAttribute("user", getPrincipal());
        return Utilities.INSTANCE.parseString(data.getAnswer()).toString();//Prof set question page.
    }
	
}
