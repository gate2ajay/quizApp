package com.project.webapp.bean;

public class Message {

    private String fullName;

    private String email;

    private String message;

    private String messageType;

    /**
     * @return the fullName
     */
    public String getFullName() {

        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {

        this.fullName = fullName;
    }

    /**
     * @return the email
     */
    public String getEmail() {

        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {

        this.email = email;
    }

    /**
     * @return the message
     */
    public String getMessage() {

        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {

        this.message = message;
    }

    /**
     * @return the messageType
     */
    public String getMessageType() {

        return messageType;
    }

    /**
     * @param messageType the messageType to set
     */
    public void setMessageType(String messageType) {

        this.messageType = messageType;
    }

}
