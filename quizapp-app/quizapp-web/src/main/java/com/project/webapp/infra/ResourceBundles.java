/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: ResourceBundles.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.infra;

public enum ResourceBundles {
    MESSAGES("messages"), EXCEPTIONS("exceptions");

    private String bundleName;

    ResourceBundles(String bundleName) {

        this.bundleName = bundleName;
    }

    public String getBundleName() {

        return bundleName;
    }

    @Override
    public String toString() {

        return bundleName;
    }
}
