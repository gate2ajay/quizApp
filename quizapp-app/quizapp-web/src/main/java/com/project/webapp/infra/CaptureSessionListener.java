/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: CaptureSessionListener.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.infra;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("captureSession")
public class CaptureSessionListener
    implements HttpSessionListener {

    /**
     * Variable to hold the object of Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(CaptureSessionListener.class);

    private static int totalActiveSessions;

    public static int getTotalActiveSession() {

        return totalActiveSessions;
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {

        updateSession(false);
        HttpSession session = event.getSession();
        LOGGER.info("SESSIONLISTEN::New Session Created with Session Id: " + session.getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {

        updateSession(true);
        HttpSession session = event.getSession();
        LOGGER.info("SESSIONLISTEN::Destroying Session with Session Id: " + session.getId());
        session.invalidate();
    }

    private static void setTotalActiveSession(int sessions) {

        synchronized (CaptureSessionListener.class) {
            totalActiveSessions = sessions;
        }
    }

    private static void updateSession(boolean destroyed) {

        synchronized (CaptureSessionListener.class) {
            setTotalActiveSession(getTotalActiveSession());
            if (destroyed) {
                totalActiveSessions = totalActiveSessions - 1;
            } else {
                totalActiveSessions = totalActiveSessions + 1;
            }
        }
    }

}
