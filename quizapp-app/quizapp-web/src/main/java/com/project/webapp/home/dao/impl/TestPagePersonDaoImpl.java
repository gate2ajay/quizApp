package com.project.webapp.home.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.project.webapp.dao.impl.AbstractDaoImpl;
import com.project.webapp.entity.Person;
import com.project.webapp.home.dao.TestPagePersonDao;

@Repository("testPagePersonDao")
public class TestPagePersonDaoImpl
    extends AbstractDaoImpl<Person, String>
    implements TestPagePersonDao {

    /**
     * Variable to hold the object of Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(TestPagePersonDaoImpl.class);

    /**
     * This constructor calls the super class constructor.
     */
    public TestPagePersonDaoImpl() {

        super();
    }

    public List<Person> getUserList() {

        // save image into database
        File file = new File("C:\\temp\\photo_id.jpg");
        byte[] bFile = new byte[(int) file.length()];

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            // convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Person personImage = new Person();
        personImage.setId(1);
        personImage.setImage(bFile);
        getCurrentSession().saveOrUpdate(personImage);

        // Get image from database
        Person person = (Person) getCurrentSession().get(Person.class, personImage.getId());
        byte[] bAvatar = person.getImage();

        try {
            FileOutputStream fos = new FileOutputStream("C:\\temp\\test.jpg");
            fos.write(bAvatar);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Query query = getCurrentSession().createQuery("from Person");
        LOGGER.debug("Fetching Persons List");

        List<Person> personList = query.list();
        personList.add(person);
        return personList;
    }

}
