package com.project.webapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.project.webapp.bean.Message;
import com.project.webapp.bean.QuestionSet;
import com.project.webapp.bean.UserBean;
import com.project.webapp.bean.UserData;
import com.project.webapp.common.Constants;
import com.project.webapp.dao.DBService;
import com.project.webapp.dao.impl.DatabaseCallsImpl;
import com.project.webapp.entity.QA;
import com.project.webapp.infra.StringSimilarity;
import com.project.webapp.person.service.HomePageService;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

@Controller
public class MainController {

    @Autowired
    HomePageService homePageService;
    
    @Autowired
    DBService dbCall;

    Map<String, String> test = new HashMap<>();

    @RequestMapping(value = {"/", "MAIN"}, method = RequestMethod.GET)
    public String defaultPage(Model model, @RequestParam(value = "logout", required = false) String logout,
                              HttpSession session) {

        model.addAttribute("title", "Main entry page.");
        model.addAttribute("message", "This is Home page!");
        if (logout != null) {
            session.invalidate();
            model.addAttribute("message", "You've been logged out successfully.");
        }
        return "views/MAIN";
    }
    
    @RequestMapping(value = "/stud", method = RequestMethod.GET)
    public String homePage(Model model) {
        model.addAttribute("user", getPrincipal());
        model.addAttribute("data", new UserData());
        return "views/select_paper";//Test Q&A page
    }
 
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(Model model) {
        model.addAttribute("user", getPrincipal());
        return "admin";//Admin add Teacher / Add Student page
    }
    
    @RequestMapping(value = "/loadTest", method = RequestMethod.POST)
    public String loadTestPaper(Model model, @ModelAttribute("data") UserData user, HttpSession session) {
        model.addAttribute("user", getPrincipal());
        List<QA> QAList = dbCall.getQuestionList(dbCall.getQuestionSetId(user.getSubject()));
        List<String> questions = new ArrayList<>();
        List<String> answers = new ArrayList<>();
        int qSetId = 0;
        for(QA qaObj : QAList) {
        	questions.add(qaObj.getQuestion());
        	answers.add(qaObj.getAnswer());
        	qSetId = qaObj.getQuestionSetId();
        }
        model.addAttribute("questionList", questions);
        model.addAttribute("data", new UserData());
        model.addAttribute("questionSet", user.getSubject());
        session.setAttribute(user.getSubject(), answers);
        return "views/paper";//Test Q&A page
    }
    
    @RequestMapping(value = "/submitPaper", method = RequestMethod.POST)
    public String submitPaper(@RequestParam("qSet")String qSet, Model model, @ModelAttribute("data") UserData user, HttpSession session) {
        model.addAttribute("user", getPrincipal());
        List<String> actualAnswers = (List<String>)session.getAttribute(qSet);
        
        List<String> studentAnswers = new ArrayList<>();
        studentAnswers.add(user.getAns1());
        studentAnswers.add(user.getAns2());
        studentAnswers.add(user.getAns3());
        studentAnswers.add(user.getAns4());
        studentAnswers.add(user.getAns5());
        
        int listSize = actualAnswers.size();
        List<Double> resultList = new ArrayList<>();
        for(int i=0; i < listSize; i++) {
        	resultList.add(StringSimilarity.similarity(actualAnswers.get(i), studentAnswers.get(i)));
        }
        double resultTotal = 0;
        for(Double result: resultList) {
        	resultTotal = resultTotal + result;
        }
        model.addAttribute("resultList", resultList);
        model.addAttribute("totalMarks", resultTotal);
        return "views/studentResult";//Test Q&A page
    }
     
    @RequestMapping(value = "/prof", method = RequestMethod.GET)
    public String dbaPage(Model model) {
        model.addAttribute("user", getPrincipal());
        model.addAttribute("qSet", new QuestionSet());
        return "views/addQSet";//Prof set question page.
    }

    @RequestMapping(value = {"/home**"}, method = RequestMethod.GET)
    public String homePage(Model model, HttpSession session) {

        String username = getUsername();
        UserBean userBean = homePageService.getUserDetails(username);
        session.setAttribute(username, userBean);
        model.addAttribute("title", "Spring Security Login Form - Database Authentication");
        model.addAttribute("message", "This is Home page!");
        /*
         * if (!InventoryStore.isDataLoaded() && InventoryStore.getInstance().getCarInventoryList()
         * != null) {
         * loadDataService.loadDataIntoDatabase(InventoryStore.getInstance().getCarInventoryList());
         * }
         */
        return "home";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout, HttpSession session) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            session.invalidate();
            model.addObject("message", "You've been logged out successfully.");
        }
        model.setViewName("user/login");
        return model;
    }

    @RequestMapping(value = {"/contactus"}, method = RequestMethod.GET)
    public String contactPage(Model model, UserBean userBean, Message message, HttpSession session) {

        model.addAttribute("title", "Contact Page");
        model.addAttribute("message", "Please coontact us through below form.");
        model.addAttribute("publicKey", Constants.RECAPTCHA_PUBLIC_KEY);
        return "contact/contactus";
    }

    @RequestMapping(value = {"/contactUs"}, method = RequestMethod.POST)
    public String contactUs(Model model, Message message, HttpServletRequest request, HttpServletResponse response,
                            HttpSession session) {

        String remoteAddr = request.getRemoteAddr();
        ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
        reCaptcha.setPrivateKey("6Lec_RQTAAAAAOmtNcGQF99EieLewLI5Fnvd4_C7");
        String challenge = request.getParameter("recaptcha_challenge_field");
        String uresponse = request.getParameter("recaptcha_response_field");
        try {
            ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
            if (reCaptchaResponse.isValid()) {
                System.out.println("Answer was entered correctly!");
                // HtmlEmailSender emailSender = new HtmlEmailSender();
                // emailSender.sendMail(message.getFullName(), message.getEmail(),
                // message.getMessage());
                model.addAttribute("success", true);
            } else {
                System.out.println("Answer is wrong");
            }
        } catch (Exception e) {
            model.addAttribute("success", false);
        }

        return "contact/contactPage";
    }

    @RequestMapping(value = {"/registerPage"}, method = RequestMethod.GET)
    public String registerPage(@RequestParam(value = "new", required = false) String newUser, Model model,
                               UserBean userBean, HttpSession session) {

        String viewName = "user/registerPage";
        if (newUser == null) {
            // handle Profile edit
            model.addAttribute("title", "CarRent Registration");
            model.addAttribute("message", "Welcome! to registration");
            viewName = "";
        } else {
            // Handle New user registration
            model.addAttribute("title", "CarRent Registration");
            model.addAttribute("message", "Welcome! to registration");
        }
        return viewName;

    }

    @RequestMapping(value = {"/newsfeed"}, method = RequestMethod.GET)
    public String getNewsFeeds(Model model, UserBean userBean, Message message, HttpSession session) {

        model.addAttribute("title", "About Page");
        model.addAttribute("active", "NEWS_FEED");
        return "views/newsfeed";
    }

    @RequestMapping(value = {"/aboutus"}, method = RequestMethod.GET)
    public String aboutUsPage(Model model, UserBean userBean, Message message, HttpSession session) {

        model.addAttribute("title", "About Page");
        model.addAttribute("active", "ABOUT_US");
        return "views/aboutus";
    }

    @RequestMapping(value = {"/contactUsPage"}, method = RequestMethod.GET)
    public String contactUpPage(Model model, UserBean userBean, Message message, HttpSession session) {

        model.addAttribute("title", "Contact Us Page");
        model.addAttribute("active", "CONTACT_US");
        return "views/contactus";
    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Login Form - Database Authentication");
        model.addObject("message", "This page is for ROLE_ADMIN only!");
        model.setViewName("admin");
        return model;

    }

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());
        }

        model.setViewName("403");
        return model;
    }

    public String getUsername() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = null;
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        return currentUserName;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));
    }

    /**
     * This method to invalidate Session on Java side.
     */
    @RequestMapping("/session_expired")
    public String invalidateSession(HttpSession session) {

        session.invalidate();
        return "invalidSession";
    }
    
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

}
