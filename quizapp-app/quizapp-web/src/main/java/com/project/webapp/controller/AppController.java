/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: CaptureController.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Apr 9, 2014
 */
package com.project.webapp.controller;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.webapp.infra.ApplicationException;
import com.project.webapp.infra.ResourceBundles;

public class AppController {

    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(AppController.class);

    /**
     * Below code initializes the Resources properties file for usage.
     */
    private final Locale locale = new Locale("en");

    private ResourceBundle messageBundle = ResourceBundle.getBundle(ResourceBundles.MESSAGES.getBundleName(), locale);

    private ResourceBundle exceptionBundle =
        ResourceBundle.getBundle(ResourceBundles.EXCEPTIONS.getBundleName(), locale);

    /**
     * @return the exceptionBundle
     */
    public ResourceBundle getExceptionBundle() {

        return exceptionBundle;
    }

    /**
     * @param exceptionBundle the exceptionBundle to set
     */
    public void setExceptionBundle(ResourceBundle exceptionBundle) {

        this.exceptionBundle = exceptionBundle;
    }

    /**
     * @return the messageBundle
     */
    public ResourceBundle getMessageBundle() {

        return messageBundle;
    }

    /**
     * @param messageBundle the messageBundle to set
     */
    public void setMessageBundle(ResourceBundle messageBundle) {

        this.messageBundle = messageBundle;
    }

    /**
     * This method to expired session Screen.
     * 
     * @param String tourName
     * @return String
     */
    @RequestMapping("/invalidSession")
    public String sessionExpiredHandler(final Model model) {

        model.addAttribute("message", "Session Expired Kindly login again..");
        return "invalidSession";
    }

    /**
     * This method to expired session Screen.
     * 
     * @param String tourName
     * @return String
     */
    @RequestMapping("/handle_error")
    public void errorHandler(@RequestParam("error") final String message) {

        LOGGER.error(message);
        LOGGER.error(getExceptionBundle().getString("EXCEPTION_MESSAGE"));
        throw new ApplicationException(getExceptionBundle().getString("EXCEPTION_MESSAGE"));
    }

    /**
     * This method to display invalid Rights screen
     */
    @RequestMapping("/invalid_rights")
    public String invalidRights(final Model model, HttpSession session) {

        model.addAttribute("message", "Access Denied: Current user does not have proper rights to view this page.");
        return "implementationInProgress";
    }
}
