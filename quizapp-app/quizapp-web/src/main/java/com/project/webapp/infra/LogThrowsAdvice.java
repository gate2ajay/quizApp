/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: LogThrowsAdvice.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.infra;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.dao.DataAccessException;

import com.project.webapp.controller.AppController;
import com.project.webapp.controller.ControllerLiterals;

@Aspect
public class LogThrowsAdvice
    implements ThrowsAdvice {

    AppController app = new AppController();

    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(LogThrowsAdvice.class);

    /**
     * Logs a message after an exception has occurred.
     * 
     * @param method The method that the exception was thrown
     * @param args The arguments to the method
     * @param target The instance containing the method
     * @param ex The exception that occurred.
     * @throws Throwable When some issue occurs logging or formatting the message
     */
    @AfterThrowing(pointcut = "execution(public * com.project.webapp.controller..*(..))", throwing = "ex")
    public void afterThrowing(JoinPoint jp, Throwable ex) {

        LOGGER.error(getStackTrace(ex));
        LOGGER.error(app.getExceptionBundle().getString(ControllerLiterals.EXEPTION_MESSAGE.getName()), ex);
        // e.getClass().getSimpleName() + " : " + e.getMessage()
        throw new ApplicationException(
            app.getExceptionBundle().getString(ControllerLiterals.EXEPTION_MESSAGE.getName()));
    }

    /**
     * Logs a message after an exception has occurred.
     * 
     * @param method The method that the exception was thrown
     * @param args The arguments to the method
     * @param target The instance containing the method
     * @param ex The exception that occurred.
     * @throws Throwable When some issue occurs logging or formatting the messageDataAccessException
     */
    @AfterThrowing(pointcut = "execution(public * com.project.webapp.controller..* (..))", throwing = "ex")
    public void afterDataException(DataAccessException ex) {

        LOGGER.error(getStackTrace(ex));
        LOGGER.error(app.getExceptionBundle().getString(ControllerLiterals.EXEPTION_MESSAGE.getName()), ex);
        throw new ApplicationException(
            app.getExceptionBundle().getString(ControllerLiterals.EXEPTION_MESSAGE.getName()));
    }

    /**
     * Returns a text representation of an exception
     * 
     * @param ex the Exception
     * @return the text
     */
    private String getStackTrace(Throwable ex) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        return sw.toString();
    }
}
