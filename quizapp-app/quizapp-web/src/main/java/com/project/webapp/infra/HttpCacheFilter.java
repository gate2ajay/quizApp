package com.project.webapp.infra;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class HttpCacheFilter
    implements Filter {

    // ---------------------------------------- ATTRIBUTS
    private static final String HEADER_GET_KEY = "Cache-Control";

    private static final String HEADER_PRAGMA = "Pragma";

    private static final String HEADER_EXPIRES = "Expires";

    private String cacheLifeTimeInstruction;

    /*
     * The values are in seconds; 216000 corresponds to two and a half days, 604800 corresponds to
     * seven full days, and 2592000 corresponds to 30 full days.
     */

    // ------------------------------------------ FONCTIONS

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
        throws IOException,
            ServletException {

        if (null != getCacheLifeTimeInstruction()) {
            ((HttpServletResponse) res).setHeader(HEADER_GET_KEY, getCacheLifeTimeInstruction());
            ((HttpServletResponse) res).setHeader(HEADER_PRAGMA, "cache");
            //
            // final int CACHE_DURATION_IN_SECOND = 60 * 60 * 24 * 5; // 5 days
            final int cacheDurationInSeconds = 2592000; // 30 days
            final long cacheDurationInMilliSeconds = cacheDurationInSeconds * 1000;
            long now = System.currentTimeMillis();
            // ((HttpServletResponse) res).setDateHeader("Last-Modified", now);
            ((HttpServletResponse) res).setDateHeader(HEADER_EXPIRES, now + cacheDurationInMilliSeconds);
        } // end-if

        chain.doFilter(req, res);
    }

    public void init(FilterConfig config) throws ServletException {

        setCacheLifeTimeInstruction(config.getInitParameter(HEADER_GET_KEY));
    }

    public void destroy() {

        setCacheLifeTimeInstruction(null);
    }

    /**
     * @return the cacheLifeTimeInstruction
     */
    public String getCacheLifeTimeInstruction() {

        return cacheLifeTimeInstruction;
    }

    /**
     * @param cacheLifeTimeInstruction the cacheLifeTimeInstruction to set
     */
    public void setCacheLifeTimeInstruction(String cacheLifeTimeInstruction) {

        this.cacheLifeTimeInstruction = cacheLifeTimeInstruction;
    }

}
