package com.project.webapp.person.service;

import com.project.webapp.bean.UserBean;

public interface HomePageService {

    public UserBean getUserDetails(String username);

    public UserBean getUserDetailsFromEmail(String username);

    public boolean isEmailAvailable(String email);

    public boolean addUser(UserBean userBean);

}
