package com.project.webapp.common;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;

public class Utilities {

    private static Utilities instance;

    /**
     * Exists only to prevent instantiation.
     */
    protected Utilities() {

        // Do nothing
    }

    public static Utilities getInstance() {

        synchronized (Utilities.class) {
            if (instance == null) {
                instance = new Utilities();
            }
            return instance;
        }
    }

    /**
     * Method to generate 4 digit pin for reset password. This code will be sent along with the
     * reset link and saved in DB for that particular user to check while the user clicks on the
     * link.
     * 
     * @return
     */
    public String generatePIN() {

        // generate a 4 digit integer 1000 <10000
        int randomPIN = (int) (Math.random() * 9000) + 1000;

        // Store integer in a string
        return String.valueOf(randomPIN);

    }

    public String getFile(String fileName) {

        StringBuilder result = new StringBuilder("");

        // Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = null;
        try {
            if (classLoader.getResource(fileName) != null) {
                file = new File(classLoader.getResource(fileName).toURI());
                result.append(file.getPath());
            } else {
                result.append("/");
            }

        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return result.toString();

    }

    public String getFileWithUtil(String fileName) {

        String result = "";

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void stop(ExecutorService executor) {

        try {
            System.out.println("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(120, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        } finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
    }

}
