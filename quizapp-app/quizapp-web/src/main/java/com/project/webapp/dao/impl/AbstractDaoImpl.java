package com.project.webapp.dao.impl;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.project.webapp.dao.AbstractDao;

public class AbstractDaoImpl<E, I extends Serializable>
    implements AbstractDao<E, I> {

    private static String schemaName = "";

    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    public AbstractDaoImpl() {

        if (getSchemaName() == null || getSchemaName().isEmpty()) {
            String schema = System.getProperty("DB.SchemaName");
            if (StringUtils.isBlank(schema)) {
                schema = "spring_db";
            }
            setSchemaName(schema);
        }
    }

    public SessionFactory getSessionFactory() {

        return sessionFactory;
    }

    @Override
    public void setSessionFactory(final SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    public Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveOrUpdate(final E e) {

        getCurrentSession().saveOrUpdate(e);
    }

    public static String getSchemaName() {

        return schemaName;
    }

    private void setSchemaName(final String schemaName) {

        AbstractDaoImpl.schemaName = schemaName;
    }

}
