package com.project.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "question_set")
public class QuestionSet {

	@Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "question_set", nullable = false)
	private String questionSet;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionSet() {
		return questionSet;
	}

	public void setQuestionSet(String questionSet) {
		this.questionSet = questionSet;
	}
	
	
	
}
