package com.project.webapp.common;

public final class Constants {

    /**
     * Exists only to prevent instantiation.
     */
    private Constants() {

    }

    public static final String RECAPTCHA_PUBLIC_KEY = "6Lec_RQTAAAAABh0VoSKY3RVDrfTOkzbrf5MEF3D";

    public static final String AVAILABLE = "AVAILABLE";

    public static final String NOT_AVAILABLE = "NOT_AVAILABLE";

    public static final String INVALID = "INVALID";

    public static final String TRUE = "TRUE";

    public static final String FALSE = "FALSE";

    public static final String FILE_CAR_INVENTORY = "data/car_details.xlsx";

    public static final String FILE_ACCESSORY_INVENTORY = "AccessoryInventory.csv";
    
    public static final String CARRIAGE_RETURN = "\r\n";

    /**
     * Technical Constants
     */

    public static final String COLLON_SEPARATOR = ":";

    public static final String SEPARATOR = ",";

    public static final String CHARSET_UTF_8 = "UTF-8";
}
