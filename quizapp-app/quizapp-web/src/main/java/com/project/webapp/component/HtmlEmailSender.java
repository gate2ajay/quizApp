package com.project.webapp.component;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class HtmlEmailSender {

    public void sendHtmlEmail(String host, String port, final String userName, final String password, String toAddress,
                              String subject, String message) throws AddressException, MessagingException {

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);

        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        // properties.put("mail.smtp.starttls.enable", "true");

        // For SSL
        /*
         * properties.put("mail.smtp.socketFactory.port", port);
         * properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
         * properties.put("mail.smtp.auth", "true"); properties.put("mail.smtp.port", port);
         */

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {

            public PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress), new InternetAddress("saneeshmg600@gmail.com"),
            new InternetAddress("ajay.tdh@gmail.com")};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        // set plain text message
        msg.setContent(message, "text/html");

        // sends the e-mail
        Transport.send(msg);

        System.out.println("Mail Sent Successfully!!");

    }

    /**
     * Test the send html e-mail method
     *
     */
    public void sendMail(String subject, String fullName, String emailId, String messageContent) {

        // SMTP server information
        String host = "anark.in"; // "smtp.gmail.com";
        String port = "25";
        String mailFrom = "ajay.raja@anark.in";
        String password = "Ajay@321";

        // outgoing message information
        if (subject.equals("")) {
            subject = "Hello my friend!!";
        }

        // message contains HTML markups
        String message = "";
        if (emailId == null && messageContent == null) {
            emailId = "ajay.tdh@gmail.com";
            message = "<i>Greetings!</i><br>";
            message += "<b>Wish you a very nice day!</b><br /><br />";
            message += "<font color=red>User Registration Successfull!!</font><br>";
            message += "<font color=red>Sent from Gmail SMTP using Java mail!!</font>";
        } else {
            message = "This message was sent by " + fullName + "<br/>" + "having email Id: " + emailId + "<br/>"
                + "and below message:<br/><br/>" + messageContent;
        }

        HtmlEmailSender mailer = new HtmlEmailSender();

        try {
            mailer.sendHtmlEmail(host, port, mailFrom, password, emailId, subject, message);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Failed to sent email.");
            ex.printStackTrace();
        }
    }
}
