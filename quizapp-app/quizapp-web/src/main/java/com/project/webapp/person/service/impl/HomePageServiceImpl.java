package com.project.webapp.person.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.webapp.bean.UserBean;
import com.project.webapp.entity.User;
import com.project.webapp.home.dao.HomePageDao;
import com.project.webapp.person.service.HomePageService;

@Transactional
@Service("homePageService")
public class HomePageServiceImpl
    implements HomePageService {

    @Autowired
    HomePageDao homePageDao;

    public UserBean getUserDetails(String username) {

        UserBean userDetailBean = new UserBean();
        User userEntity = homePageDao.getUserDetails(username);
        if (userEntity == null) {
            userDetailBean = null;
        } else {
            userDetailBean.setId(userEntity.getId());
            userDetailBean.setAddress(userEntity.getAddress());
            userDetailBean.setCity(userEntity.getCity());
            userDetailBean.setEmail(userEntity.getEmail());
            userDetailBean.setFullName(userEntity.getFirstName());
            userDetailBean.setMobile(userEntity.getMobile());
            userDetailBean.setNewsLetter(userEntity.isNewsLetter());
            userDetailBean.setUsername(userEntity.getUsername());
            userDetailBean.setPassword(userEntity.getPassword());
            userDetailBean.setResetCode(userEntity.getResetCode());
            userDetailBean.setResetStatus(userEntity.isResetStatus());
        }
        return userDetailBean;
    }

    public UserBean getUserDetailsFromEmail(String email) {

        UserBean userDetailBean = new UserBean();
        User userEntity = homePageDao.getUserDetailsFromEmail(email);
        if (userEntity == null) {
            userDetailBean = null;
        } else {
            userDetailBean.setId(userEntity.getId());
            userDetailBean.setAddress(userEntity.getAddress());
            userDetailBean.setCity(userEntity.getCity());
            userDetailBean.setEmail(userEntity.getEmail());
            userDetailBean.setFirstName(userEntity.getFirstName());
            userDetailBean.setLastName(userEntity.getLastName());
            userDetailBean.setMobile(userEntity.getMobile());
            userDetailBean.setNewsLetter(userEntity.isNewsLetter());
            userDetailBean.setUsername(userEntity.getUsername());
            userDetailBean.setPassword(userEntity.getPassword());
            userDetailBean.setResetCode(userEntity.getResetCode());
            userDetailBean.setResetStatus(userEntity.isResetStatus());
        }
        return userDetailBean;
    }

    public boolean isEmailAvailable(String email) {

        return homePageDao.checkEmail(email);
    }

    public boolean addUser(UserBean userBean) {

        User userEntity = new User();
        userEntity.setId(userBean.getId());
        userEntity.setAddress(userBean.getAddress());
        userEntity.setCity(userBean.getCity());
        userEntity.setEmail(userBean.getEmail());
        userEntity.setFirstName(userBean.getFirstName());
        userEntity.setLastName(userBean.getLastName());
        userEntity.setMobile(userBean.getMobile());
        userEntity.setPassword(userBean.getPassword());
        userEntity.setUsername(userBean.getUsername());
        userEntity.setNewsLetter(userBean.isNewsLetter());
        userEntity.setEnabled(true);
        if (userBean.getResetCode() != null) {
            userEntity.setResetCode(userBean.getResetCode());
            userEntity.setResetStatus(userBean.isResetStatus());
        }
        return homePageDao.saveUser(userEntity);
    }

    public void updateForResetPass(String email, String resetCode) {

    }
}
