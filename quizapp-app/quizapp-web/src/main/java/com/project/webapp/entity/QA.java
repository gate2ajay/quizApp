package com.project.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "qa")
public class QA {

	@Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "question", nullable = false)
	private String question;
	
	@Column(name = "answer", nullable = false)
	private String answer;
	
	@Column(name = "question_set", nullable = false)
	private int questionSetId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(int questionSetId) {
		this.questionSetId = questionSetId;
	}
	
	
	
}
