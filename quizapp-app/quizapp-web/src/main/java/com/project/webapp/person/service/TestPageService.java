package com.project.webapp.person.service;

import java.util.List;

import com.project.webapp.bean.UserBean;

public interface TestPageService {

    public List<UserBean> getUserList();
}
