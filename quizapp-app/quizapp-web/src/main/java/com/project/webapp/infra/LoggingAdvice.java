/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: LoggingAdvice.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.infra;

import java.lang.reflect.Field;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.project.webapp.bean.UserBean;
import com.project.webapp.controller.ControllerLiterals;
import com.project.webapp.security.CaptureSecurityManager;

@Aspect
public class LoggingAdvice {

    @Autowired
    private CaptureSecurityManager securityManager;

    /** Logger */
    private static final Logger LOGGER = Logger.getLogger(LoggingAdvice.class);

    @Around("execution(public * com.project.webapp..*(..))")
    public Object aopLogging(final ProceedingJoinPoint pjp) throws Throwable {

        long start = System.currentTimeMillis();
        LOGGER.info("Inside method = " + pjp.getSignature().getName());
        LOGGER.info("Agruments Passed = " + Arrays.toString(pjp.getArgs()));
        LOGGER.debug("Inside method = " + pjp.getSignature().getName());
        LOGGER.debug("Agruments Passed = " + Arrays.toString(pjp.getArgs()));
        Object output = pjp.proceed();
        LOGGER.info("End of method <<<");
        LOGGER.info("Method '" + pjp.getSignature().getName() + "' took" + (System.currentTimeMillis() - start)
            + " milliseconds");
        LOGGER.debug("End of method <<<");
        LOGGER.debug("Method '" + pjp.getSignature().getName() + "' took" + (System.currentTimeMillis() - start)
            + " milliseconds");
        return output;
    }

    @Before("execution(public * com.project.webapp.controller..*(..))")
    public void loadInitialData(final JoinPoint jp) throws Exception {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();

        HttpSession session = request.getSession(false);
        if (session == null) {
            LOGGER.info("AOP::Session Not Available, session is null");
            LOGGER.info("AOP::Creating New Session");
            session = request.getSession();
            LOGGER.info("AOP::Created New Session and Session ID is: " + request.getSession().getId());

        } else {
            LOGGER.info("AOP::Session Available " + request.getSession().getId());
        }
        UserBean userBean = (UserBean) session.getAttribute("userDetail");
        String screenId = getScreenId(jp);
        if (userBean != null) {
            session.setAttribute("userDetail", userBean);
            session.setAttribute(ControllerLiterals.RIGHT_ID.getName(), 3);
        } else {
            securityManager.initializeUserSession(attr, screenId);
            userBean = (UserBean) session.getAttribute("userDetail");
            session.setAttribute(ControllerLiterals.RIGHT_ID.getName(), 3);
        }
    }

    public String getScreenId(final JoinPoint jp) throws IllegalAccessException {

        Field objectField = null;
        String fieldName = "", screenId = "";
        try {
            objectField = jp.getTarget().getClass().getField("SCREEN_ID");
        } catch (NoSuchFieldException nsfe) {
            nsfe.getMessage();
        }
        if (objectField == null) {
            fieldName = "";
        } else {
            fieldName = objectField.getName();
        }
        if (fieldName.equals("SCREEN_ID")) {
            try {
                screenId = objectField.get(null).toString();
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException(
                    "IllegalArgumentException occured while getting screen ID : " + e.getMessage(), e);
            }
        } else {
            screenId = "";
        }
        return screenId;
    }

}
