package com.project.webapp.home.dao;

import com.project.webapp.entity.User;

public interface HomePageDao {

    public User getUserDetails(String username);

    public User getUserDetailsFromEmail(String username);

    public boolean checkEmail(String email);

    public boolean saveUser(User userEntity);

}
