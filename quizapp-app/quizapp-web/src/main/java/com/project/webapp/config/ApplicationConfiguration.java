package com.project.webapp.config;

import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.project.webapp.entity" })
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {

	org.apache.log4j.Logger Logger = org.apache.log4j.Logger.getLogger(ApplicationConfiguration.class);
	
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.project.webapp.entity" });
        sessionFactory.setHibernateProperties(hibProperties());
        return sessionFactory;
     }

    @Bean
    public DataSource dataSource() {
    	DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/spring_db");
        dataSource.setUsername("shalom");
        dataSource.setPassword("Shalom@123");
        return dataSource;
    }
    private Properties hibProperties() {
        Properties properties = new Properties();
        properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
        properties.put(Environment.SHOW_SQL, true);
        /*properties.put(Environment.USE_SECOND_LEVEL_CACHE,true);
        properties.put(Environment.CACHE_REGION_FACTORY,"org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
        properties.put(Environment.CACHE_PROVIDER_CONFIG,"/ehcache.xml");
        properties.put(Environment.USE_QUERY_CACHE, true);*/
        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
       HibernateTransactionManager txManager = new HibernateTransactionManager();
       txManager.setSessionFactory(s);
       return txManager;
    }
    
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

        // http
        HttpMessageConverter converter = new StringHttpMessageConverter();
        converters.add(converter);
        Logger.info("HttpMessageConverter added");

        // string
        converter = new FormHttpMessageConverter();
        converters.add(converter);
        Logger.info("FormHttpMessageConverter added");

        // json
        converter = new MappingJackson2HttpMessageConverter();
        converters.add(converter);
        Logger.info("MappingJackson2HttpMessageConverter added");

    }
}