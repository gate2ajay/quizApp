package com.project.webapp.home.dao;

import java.util.List;

import com.project.webapp.entity.Person;

public interface TestPagePersonDao {

    public List<Person> getUserList();
}
