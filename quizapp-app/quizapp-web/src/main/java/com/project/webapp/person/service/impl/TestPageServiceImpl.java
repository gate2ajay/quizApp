package com.project.webapp.person.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.webapp.bean.UserBean;
import com.project.webapp.entity.Person;
import com.project.webapp.home.dao.TestPagePersonDao;
import com.project.webapp.person.service.TestPageService;

@Transactional
@Service("testPageService")
public class TestPageServiceImpl
    implements TestPageService {

    @Autowired
    TestPagePersonDao testPagePersonDao;

    public List<UserBean> getUserList() {

        UserBean userDetailBean = new UserBean();
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        List<Person> userDetailList = testPagePersonDao.getUserList();
        ListIterator<Person> resultListIterator = userDetailList.listIterator();
        while (resultListIterator.hasNext()) {
            Person personObj = resultListIterator.next();
            userDetailBean.setAddress(personObj.getAddress());
            userDetailBean.setCity(personObj.getCity());
            userBeanList.add(userDetailBean);
        }
        return userBeanList;
    }
}
