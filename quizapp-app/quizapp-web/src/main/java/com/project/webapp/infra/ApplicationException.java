/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: ApplicationException.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Aug 19, 2014
 */
package com.project.webapp.infra;

/**
 * Capture Exception
 * 
 */

public class ApplicationException
    extends RuntimeException {

    /**
     * Unique ID for Serialized object
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * String message
     * Line 33 will throw SONAR
     * Severity : Major
     * Issue : Mutable Exception
     * Cause : The field 'message' must be declared final.
     * Reason for not handling : we are sending appropriate messages to constructor while capturing the exception,thus can not make the field final though it is based on design rule.
     */
    private String message;

    /**
     * default constructor
     * 
     */
    public ApplicationException() {

        super();
    }

    /**
     * @param message
     * 
     */
    public ApplicationException(String message) {

        super();
        this.message = message;
    }

    /**
     * @return the message
     * 
     */
    public String getMessage() {

        return message;
    }

    /**
     * @param message the message to set
     * 
     */
    public void setMessage(String message) {

        this.message = message;
    }
    /**
     * @future reference
     * 
     */
}
