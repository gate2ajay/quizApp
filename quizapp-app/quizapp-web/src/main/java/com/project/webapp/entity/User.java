package com.project.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "first_name", nullable = false)
    String firstName;

    @Column(name = "last_name", nullable = false)
    String lastName;

    @Column(name = "username", nullable = false)
    String username;

    @Column(name = "password", nullable = false)
    String password;

    @Column(name = "enabled", nullable = false)
    boolean enabled;

    @Column(name = "email", nullable = false)
    String email;

    @Column(name = "address", nullable = false)
    String address;

    @Column(name = "mobile", nullable = false)
    String mobile;

    @Column(name = "city", nullable = false)
    String city;

    @Column(name = "news_letter", nullable = true)
    boolean newsLetter;

    @Column(name = "reset_status")
    boolean resetStatus;

    @Column(name = "reset_code")
    String resetCode;

    /**
     * @return the id
     */
    public long getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {

        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {

        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {

        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    /**
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {

        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {

        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {

        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {

        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {

        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {

        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {

        this.address = address;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {

        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {

        this.mobile = mobile;
    }

    /**
     * @return the city
     */
    public String getCity() {

        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {

        this.city = city;
    }

    /**
     * @return the newsLetter
     */
    public boolean isNewsLetter() {

        return newsLetter;
    }

    /**
     * @param newsLetter the newsLetter to set
     */
    public void setNewsLetter(boolean newsLetter) {

        this.newsLetter = newsLetter;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {

        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {

        this.enabled = enabled;
    }

    /**
     * @return the resetStatus
     */
    public boolean isResetStatus() {

        return resetStatus;
    }

    /**
     * @param resetStatus the resetStatus to set
     */
    public void setResetStatus(boolean resetStatus) {

        this.resetStatus = resetStatus;
    }

    /**
     * @return the resetCode
     */
    public String getResetCode() {

        return resetCode;
    }

    /**
     * @param resetCode the resetCode to set
     */
    public void setResetCode(String resetCode) {

        this.resetCode = resetCode;
    }

}
