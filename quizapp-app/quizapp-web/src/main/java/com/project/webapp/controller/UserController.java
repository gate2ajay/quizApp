package com.project.webapp.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.webapp.bean.UserBean;
import com.project.webapp.common.Utilities;
import com.project.webapp.component.HtmlEmailSender;
import com.project.webapp.person.service.HomePageService;

@Controller
public class UserController {

    @Autowired
    HomePageService homePageService;

    @RequestMapping(value = {"/registerUser"}, method = RequestMethod.POST)
    public String registerUser(Model model, UserBean userBean, HttpSession session) {

        model.addAttribute("title", "Spring Security Login Form - Database Authentication");
        model.addAttribute("message", "This is Home page!");
        UserBean userBeanFromDB = homePageService.getUserDetails(userBean.getUsername());
        if (userBeanFromDB == null) {
            // No user by the username in DB, So Checking for email
            boolean checkEmail = homePageService.isEmailAvailable(userBean.getEmail());
            if (checkEmail) {
                // Email already registered with us. Click on forgot password to reset password.
                model.addAttribute("accountPresent", true);
                return "user/registerPage";
            }

            // No account with username or email present in DB, So, proceed with new user addition.
            boolean userSaved = homePageService.addUser(userBean);
            if (userSaved) {
                // Redirect to Login screen
                model.addAttribute("userRegister", "success");
                return "login";
            } else {
                model.addAttribute("userRegister", "failure");
            }
        } else {
            model.addAttribute("accountPresent", true);
            return "user/registerPage";
        }
        model.addAttribute("userBean", userBean);
        return "user/registerPage";

    }

    @RequestMapping(value = {"/forgot_page"}, method = RequestMethod.GET)
    public String registerPage(Model model, HttpSession session) {

        // Handle New user registration
        model.addAttribute("title", "Username / Password Reset Page");
        model.addAttribute("message", "Please enter your registered e-mail Id to receive reset link.");
        return "user/forgotPage";

    }

    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    public String forgotPass(Model model, @RequestBody UserBean userBean) {

        UserBean tempUserBean = homePageService.getUserDetailsFromEmail(userBean.getEmail());
        if (tempUserBean != null) {
            String resetCode = Utilities.getInstance().generatePIN();
            // update user in DB with password reset code and status.
            tempUserBean.setResetCode(resetCode);
            tempUserBean.setResetStatus(true);
            homePageService.addUser(tempUserBean);

            // To-Do
            // Send mail to the registered User with the password reset Link.
            String link = "http://mfriend.in/resetpass.htm?action=reset&user=" + tempUserBean.getUsername()
                + "&resetCode=" + resetCode;
            HtmlEmailSender emailSender = new HtmlEmailSender();
            String subject = "m' friend Password Reset";
            emailSender.sendMail(subject, tempUserBean.getFullName(), tempUserBean.getEmail(),
                createPassResetMessage(tempUserBean.getFullName(), link));

            model.addAttribute("title", "Username / Password Reset Page");
            model.addAttribute("message",
                "Your request to reset password was successfully sent to your registered e-mail id.");
            model.addAttribute("submitted", true);
        } else {
            model.addAttribute("title", "Username / Password Reset Page");
            model.addAttribute("message", "Please enter registered e-mail id.");
            model.addAttribute("submitted", false);
        }
        return "user/forgotPage";
    }

    public String createPassResetMessage(String fullName, String link) {

        StringBuilder message = new StringBuilder("Hi " + fullName);
        message.append("<br><br>");
        message.append("You recently requested to reset your m' friend password. To complete your request, "
            + "please take one of the following actions: <br>");

        message.append("<ul><li>Click on below link to reset password.<br>" + "<a href='" + link
            + "'>Click her to reset password.</a></li>"
            + "<li>Copy paste the below url, hit enter/home key and reset you password.<br>" + link + "</li>"
            + "</ul><br><br><br>");
        message.append("-- Car Rental Team.");
        return message.toString();
    }

    @RequestMapping(value = {"/resetpass"}, method = RequestMethod.GET)
    public String resetPassLink(Model model, @RequestParam("action") String action,
                                @RequestParam("user") String username, @RequestParam("resetCode") String resetCode) {

        UserBean userBeanFromDB = homePageService.getUserDetails(username);
        String resetCodeFromDB = userBeanFromDB.getResetCode();
        if (userBeanFromDB != null && resetCodeFromDB != null) {
            if (resetCodeFromDB.equals(resetCode)) {
                model.addAttribute("userBean", userBeanFromDB);
                model.addAttribute("message", "Please enter new password.");
                return "user/resetPass";
            }
        }
        model.addAttribute("title", "Could not reset password.");
        return "user/forgotPage";

    }

    @RequestMapping(value = {"/set_pass"}, method = RequestMethod.POST)
    public String setPassword(Model model, UserBean userBean) {

        UserBean tempUserBean = homePageService.getUserDetails(userBean.getUsername());
        if (tempUserBean == null) {
            model.addAttribute("noSuchUser", true);
            return "user/register";
        } else {
            tempUserBean.setPassword(userBean.getPassword());
            tempUserBean.setResetCode(null);
            tempUserBean.setResetStatus(false);
            homePageService.addUser(tempUserBean);
            model.addAttribute("passwordReset", "success");
            return "user/login";
        }

    }
}
