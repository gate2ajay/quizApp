package com.project.webapp.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.webapp.adapters.HTMLCharacterEscapes;
import com.project.webapp.bean.UserBean;
import com.project.webapp.person.service.TestPageService;

@Controller
public class TestPageController {

    @Autowired
    TestPageService testPageService;

    @RequestMapping(value = "/test")
    public final String testPageLanding(Model model, TestBean testBean) {

        return "login";
    }

    @RequestMapping(value = "/testForm", method = RequestMethod.POST)
    public final String testFormSubmit(TestBean testBean, @RequestParam(value = "myParam") String myParam,
                                       Model model) {

        model.addAttribute("testParam", myParam);
        model.addAttribute("msgFromBean", testBean.getMessage());

        List<UserBean> userBeanList = testPageService.getUserList();
        model.addAttribute("userBeanList", userBeanList);
        makeRestCall();

        return "testFormSubmitPage";
    }

    public void makeRestCall() {

        RestTemplate restTemplate = new RestTemplate();
        UserBean bean = restTemplate.getForObject("http://localhost:8085/spring-web/getUser.html", UserBean.class);
        try {
            FileOutputStream fos = new FileOutputStream("C:\\temp\\test2.jpg");
            // fos.write(bean.getImage());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/getUser")
    public ResponseEntity<String> getUser(Model model) throws IOException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=utf-8");

        List<UserBean> userBeanList = testPageService.getUserList();
        UserBean userWithImage = userBeanList.get(userBeanList.size() - 1);
        model.addAttribute("userBeanList", userBeanList);

        return new ResponseEntity<String>(toJson(userWithImage), responseHeaders, HttpStatus.OK);
    }

    @SuppressWarnings("deprecation")
	private String toJson(UserBean bean) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        mapper.getJsonFactory().setCharacterEscapes(new HTMLCharacterEscapes());

        try {
            return mapper.writeValueAsString(bean);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
