package com.project.webapp.adapters;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.event.ContextRefreshedEvent;

public class ServletContextAttributeInitializer
    implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private MessageSource messageSource;

    public void onApplicationEvent(ContextRefreshedEvent event) {

        servletContext.setAttribute("msg", new MessageSourceMapAdapter(messageSource));
        servletContext.setAttribute("cp", servletContext.getContextPath());
    }
}
