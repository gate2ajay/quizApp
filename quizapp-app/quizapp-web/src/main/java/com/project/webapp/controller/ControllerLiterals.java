/**
 * Eandis Confidential Copyright 2014 by Eandis or its affiliates. All rights reserved.
 * 
 * @Workfile: ControllerLiterals.java
 * @Revision: 1.0
 * @Author: Accenture IDC
 * @ModDatetime: Apr 9, 2014
 */
package com.project.webapp.controller;

public enum ControllerLiterals {

    /**
     * Common Literals and property keys
     */
    NAME("name"), FROM_PAGE("fromPage"), RIGHT_ID("rightId"), SCREEN_MAP("screenMap"), SCREEN_ID("screenId"),
    EXEPTION_MESSAGE("EXCEPTION_MESSAGE");

    private final String name;

    private ControllerLiterals(String s) {

        name = s;
    }

    public String getName() {

        return name;
    }

    public boolean equalsName(String newName) {

        return (newName == null) ? false : name.equals(newName);
    }

    public String toString() {

        return name;
    }
}
