package com.project.webapp.dao;

import java.io.Serializable;

import org.hibernate.SessionFactory;

public interface AbstractDao<E, I extends Serializable> {

    void saveOrUpdate(E e);

    void setSessionFactory(SessionFactory sessionFactory);
}
