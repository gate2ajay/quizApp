package com.project.webapp.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.webapp.dao.DBService;
import com.project.webapp.entity.QA;

@Transactional
@Repository("dbCall")
public class DatabaseCallsImpl extends AbstractDaoImpl<Object, String> implements DBService {
	
	public DatabaseCallsImpl() {

        super();
    }
	
	private static final Logger LOGGER = Logger.getLogger(DatabaseCallsImpl.class);
	
	public int getQuestionSetId(String questionSet) {
		Query query = getCurrentSession().createQuery("select id from QuestionSet where questionSet=:questionSet");
		query.setParameter("questionSet", questionSet);
		LOGGER.debug("question Set: "+questionSet);
		return (int)query.uniqueResult();
	}

	public List<QA> getQuestionList(int qSetId) {
		Query query = getCurrentSession().createQuery("from QA where questionSetId=:qSetId");
        query.setParameter("qSetId", qSetId);
        LOGGER.debug("QSetID: "+qSetId);
        return query.list();
	}

}
