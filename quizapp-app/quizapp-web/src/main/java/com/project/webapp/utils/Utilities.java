package com.project.webapp.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

public enum Utilities {
	INSTANCE;

    /**
     * Variable to hold the object of Logger.
     */
    Logger LOGGER = Logger.getLogger(Utilities.class);
	
	

	public List<String> parseString(String input) {
		Scanner fileScan= new Scanner(input).useDelimiter("[ ,!?.']+");
	    String word;
	    List<String> outputStringList = new ArrayList<>();
	    while(fileScan.hasNext()){       
	        word= fileScan.next();
	        outputStringList.add(word);
	    }
	    fileScan.close();
	    return outputStringList;
	}
}
