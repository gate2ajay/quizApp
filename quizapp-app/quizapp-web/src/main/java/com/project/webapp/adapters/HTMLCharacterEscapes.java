/**
 * 
 */
package com.project.webapp.adapters;

import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.SerializedString;

/**
 * @author v.shamrao.niungare
 * 
 */
public class HTMLCharacterEscapes
    extends CharacterEscapes {

    private final int[] asciiEscapes;

    /**
     * 
     */

    public HTMLCharacterEscapes() {

        // start with set of characters known to require escaping (double-quote, backslash etc)
        super();
        int[] esc = CharacterEscapes.standardAsciiEscapesForJSON(); // and force escaping of a few
                                                                    // others:
        esc['<'] = CharacterEscapes.ESCAPE_CUSTOM;
        esc['>'] = CharacterEscapes.ESCAPE_CUSTOM;
        asciiEscapes = esc;

    }

    /*
     * (non-Javadoc)
     * @see org.codehaus.jackson.io.CharacterEscapes#getEscapeCodesForAscii()
     */
    @Override
    public int[] getEscapeCodesForAscii() {

        // return asciiEscapes;
        return (int[]) asciiEscapes.clone();
    }

    /*
     * (non-Javadoc)
     * @see org.codehaus.jackson.io.CharacterEscapes#getEscapeSequence(int)
     */
    @Override
    public SerializableString getEscapeSequence(int arg0) {

        if (arg0 == 60) {
            return new SerializedString("&lt;");
        }

        if (arg0 == 62) {
            return new SerializedString("&gt;");
        }

        return null;
    }

}
