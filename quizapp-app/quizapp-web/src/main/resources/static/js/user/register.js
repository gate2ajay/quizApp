var screenBundleEan = {
		       1:'Account already present. Click <a href="forgot_page.htm">here</a> to reset username/password.',
		       
};

$(document).ready(function() {
	$("#registerUser").validate({
		rules: {
			  username: {
				  required: true,
				  minlength: 4,
				  maxlength : 15,
			  },
			  password: {
				  required : true,
				  minlength: 4
			  },
			  password2: {
				  required : true,
				  minlength: 4,
				  equalTo: '#password',
			  },
			  name: {
				  required: true,
				  minlength: 4,
				  maxlength: 50,
			  },
			  email: {
				  required: true,
				  email: true
			  },
			  address: 'required',
			  phone: {
				  required: true,
				  number: true
			  }
		   },
		   messages : {
			   username: {
				   required : 'Please enter username', 
				   minlength : 'Username should not be less than 8 characters.',
				   maxlength : 'Username should not be greater than 15 characters.',
			   },
			   password: {
				   required : 'Please enter a password',
				   minlength : 'Password should be minimum 5 characters long.',
			   },
			   password2: {
				   required : 'Please enter a confirm password.',
				   minlength : 'Confirm password should be minimum 5 characters long.',
				   equalTo: 'Confirm password should be same as above.',
			   },
			   name: {
					  required: 'Please enter your full name.',
					  maxlength: 'Maximum 50 characters allowed.',
			   },
			   email: {
					  required: 'Please enter a e-mail address.',
					  email: 'Please enter a valid email address.',
			   },
           },
	});
	
	if(accountPresent) {
		showAlert(mainScreenBundle.valueOf()[1], screenBundleEan.valueOf()[1]);
	}
});